<?php

declare(strict_types=1);

use App\Http\Controllers\Auth\UserController;
use App\Payments\Moyasar;
use App\Payments\MyFatoorah;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\BankAccountController;
use App\Http\Controllers\DonateRequestsController;
use App\Http\Controllers\ProjectCategoryController;
use Illuminate\Support\Facades\Http;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::middleware([
    'web',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])->group(function () {
    Route::get('/debug', function () {

        // return 'debug'
        $baseDomain = 'ghaith.io';
        $parts = explode('.', $baseDomain);
        $rootDomain = $parts[count($parts) - 2] . '.' . $parts[count($parts) - 1];
        $domain = 'createdbyapi';
        $t_domain = "$domain.$rootDomain";

        $cpanelHost = 'https://172-104-229-178.ip.linodeusercontent.com:2083';
        // $apiToken = '62AASUX7Q6F2W80WJDX0VR2X59OE2R17';
        $cpanelUsername = 'ghaithstores';
        $cpanelPassword = 'U+QdC%F5wSxw';

        $apiUrl = "{$cpanelHost}/json-api/cpanel";

        // return $t_domain;
        $response = Http::withBasicAuth($cpanelUsername, $cpanelPassword)
            ->post($apiUrl, [
                'cpanel_jsonapi_apiversion' => 2,
                // 'cpanel_jsonapi_module' => 'Park',
                // 'cpanel_jsonapi_func' => 'park',
                // 'domain' => 'created.ghaith.io',
                // 'topdomain' => 'stores.ghaith.io',

                'cpanel_jsonapi_module' => 'AddonDomain',
                'cpanel_jsonapi_func' => 'addaddondomain',
                'domain' => 'created.bebo2xd.site',
                'subdomain' => 'created', // You can adjust this as needed
                'dir' => '/public_html', // You can adjust this as needed
                // 'pass' => 'your-addon-domain-password', // Set a password for the addon domain
                'newdomain' => 'stores.ghaith.io',

            ]);

        return $response->json();

        return $response;





    });

    Route::get('/', [HomeController::class, 'index']);
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::get(
        '/categories/{projectCategory}',
        [ProjectCategoryController::class, 'index']
    )->name('project-category.index');

    Route::get('/projects', [ProjectController::class, 'index'])->name('projects.index');
    Route::get('/gift-projects', [ProjectController::class, 'gift'])->name('projects.gift');
    Route::get('/projects/{project}', [ProjectController::class, 'show'])->name('projects.show');


    Route::get('/bank-accounts', [BankAccountController::class, 'index'])->name('bank-accounts.index');
    Route::view('/zakat-calculator', 'web.zakat-calculator')->name('zakat-calculator');
    Route::view('/contact-us', 'web.contact-us')->name('contact-us');
    Route::post('/contact-us', [ContactUsController::class, 'store'])->name('contact-us');


    Route::view('/used-furniture-donation', 'web.used-furniture-donate')->name('used-furniture-donation');
    Route::post('/used-furniture-donation', [DonateRequestsController::class, 'store'])->name('send-used-furniture-donation');


    Route::get('/cart', [CartController::class, 'index'])->name('cart');
    Route::post('/add-to-cart', [CartController::class, 'store'])->name('add-to-cart');
    Route::post('/remove-from-cart', [CartController::class, 'destroy'])->name('remove-from-cart');
    Route::post('/request-pay', [CartController::class, 'requestPay'])->name('request-pay');

    Route::post('/quick-donate', [CartController::class, 'quickDonate'])->name('quick-donate');

    Route::get('/login', function () {
        return redirect()->route('home');
    });

    Route::post('/login', [LoginController::class, 'login'])->name('login');

    Route::middleware('auth')->group(function () {
        Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
    });

    Route::view('/moyasar', 'payments.moyasar')->name('moyasar.pay');

    Route::prefix('callbacks')->group(function () {
        Route::get('/myfatoorah', [MyFatoorah::class, 'callback'])->name('myfatoorah.callback');
        Route::get('/moyasar', [Moyasar::class, 'callback'])->name('moyasar.callback');
    });

    Route::middleware('auth')->group(function () {
        Route::get('account', [UserController::class, 'show'])->name('profile');
        Route::post('account', [UserController::class, 'update'])->name('update-profile');
    });
});
