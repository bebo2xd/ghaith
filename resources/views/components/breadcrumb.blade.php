<section class="mt-8">
    <nav aria-label="breadcrumb" class="breadcrumb-bg">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('home')}}">الرئيسية</a>
                </li>
                {{ $slot }}
            </ol>
        </div>
    </nav>
</section>