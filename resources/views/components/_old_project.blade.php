@props([
    'long_description' => false,
    'project',
])

<div class="project-box @if ($project->isDone()) project-box-done p-done @endif" x-data="{
    increment: 1,
    price: 1,
    totalPrice: 1,
    calculateTotalPrice() { this.totalPrice = this.price * this.increment }
}">
    <div class="img">
        <div class="above-img">
            <a href="{{ route('project-category.index', $project->project_category_id) }}"
                class="btn above-img-bt-bg">{{ $project->category->title }}</a>
            <span style="cursor: pointer" onclick="showShareURL('{{ route('projects.show', $project) }}')">
                <img src="{{ asset('/assets/img/share.svg') }}">
            </span>
        </div>
        <a href="{{ route('projects.show', $project) }}">
            <img class="img-fluid project-img" alt="Project" src="{{ $project->logo_url }}" />
        </a>
    </div>
    <div class="donation-info">
        <a href="{{ route('projects.show', $project) }}">
            <h3>{{ $project->title }}</h3>
        </a>
        <div class="showdesc m-0">
            @if ($long_description)
                {!! $project->description !!}
            @else
                {!! $project->short_description !!}
                <a href="{{ route('projects.show', $project) }}"
                    style="cursor:pointer; text-decoration:underline !important;" class="text-primary">
                    ...
                </a>
            @endif
        </div>
    </div>
    <div class="donation-amount-main" @if ($project->forDonate()) style="visibility: hidden;"> @endif>
        <div class="d-amount-price">
            @foreach ($project->shares as $share)
                <button class="btn share" x-on:click="increment=1;price={{ $share['price'] }};calculateTotalPrice()">
                    {{ $share['title'] }}
                </button>
            @endforeach
        </div>
        @if ($project->is_goalable)
            <div role="progressbar" aria-valuenow="{{ $project->complete_percent }}" aria-valuemin="0"
                aria-valuemax="100" style="--value:{{ $project->complete_percent }}">
            </div>
        @endif
    </div>
    <div class="amount-offer" @if ($project->forDonate()) style="visibility: hidden;"> @endif>

        @if ($project->is_goalable)
            <div class="amount-offer-inr d-flex align-center">
                <img src="{{ asset('/assets/img/target.svg') }}">
                <h3>الهدف:</h3>
                <span>{{ $project->goal }}</span>
            </div>
            <div class="amount-offer-inr d-flex align-center am-inr">
                <img src="{{ asset('/assets/img/discount.svg') }}">
                <h3>المتبقي: </h3>
                <span id="changepr">
                    {{ $project->remaining }}
                </span>
            </div>
        @endif
    </div>
    <div class="amount-last" @if ($project->forDonate()) style="visibility: hidden;"> @endif>
        <div class="input-group increment-group">
            <span class="input-group-append">
                <button type="button"
                    @if (!$project->isDone()) x-on:click="increment++;calculateTotalPrice()" @endif
                    class="btn btn-outline-secondary btn-number">
                    <span class="fa fa-plus"></span>
                </button>
            </span>
            <span class="input-group-prepend">
                <button type="button"
                    @if (!$project->isDone()) x-on:click="(increment==1)? 1 : increment--;calculateTotalPrice()" @endif
                    class="btn btn-outline-secondary btn-number">
                    <span class="fa fa-minus"></span>
                </button>
            </span>
            <input type="number" min="1" max="{{ $project->price }}" x-model="increment"
                class="form-control input-number">
        </div>
        <div class="amount-group">
            <input type="number" class="text-dark add-am-bt" id="famount" name="famount" x-model="totalPrice"
                @change="price=event.target.value;increment=1" min="1" placeholder="أضف المبلغ">
        </div>
        <div class="amount-basket bas-left">
            <button class="btn @if (!$project->isDone()) add-to-cart @endif" href="javascript:void(0)"
                data-project-id="{{ $project->id }}" x-bind:data-price="totalPrice" x-bind:data-amount="increment"
                style="width:40px">
                <img src="{{ asset('/assets/img/add_cart.svg') }}">
            </button>
        </div>
        @if ($project->is_giftable)
            <div class="amount-basket">
                <button class="btn @if (!$project->isDone()) add-gift-to-cart @endif" href="javascript:void(0)"
                    data-toggle="modal" data-target=".login-popup" data-project-id="{{ $project->id }}"
                    x-bind:data-price="totalPrice" x-bind:data-amount="increment" style="width:40px">
                    <img src="{{ asset('/assets/img/gift.svg') }}">
                </button>
            </div>
        @endif

    </div>
    @if ($project->isDone())
        <div class="project-done">
            <b>مكتمل</b>
        </div>
    @endif
</div>
