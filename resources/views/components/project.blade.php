@props([
    'long_description' => false,
    'project',
])

<div class="bg-black-05 black hover-brand grow style-module--wrapper--2E4lN my-3 @if ($project->isDone()) project-box-done p-done @endif"
    x-data="{
        increment: 1,
        price: 1,
        totalPrice: 1,
        calculateTotalPrice() { this.totalPrice = this.price * this.increment }
    }">
    <div class="p-img">
        <div class="above-img">
            <a href="{{ route('project-category.index', $project->project_category_id) }}"
                class="btn above-img-bt-bg clr-primary">{{ $project->category->title }}</a>
            <span style="cursor: pointer" onclick="showShareURL('{{ route('projects.show', $project) }}')">
                <img src="{{ asset('/assets/img/share.svg') }}">
            </span>
        </div>
        <img class="project-img" src="{{ $project->logo_url }}">
    </div>
    <div class="absolute--fill bg-black-05 ba b--black-05"></div>
    <div class="pa3 tr">
        @if ($project->isDone())
            <div class="project-done">
                <b>مكتمل</b>
            </div>
        @endif

        <h5 class="mv0 f5 fw6 lh-copy truncate-ns d-flex justify-between">
            <a href="{{ route('projects.show', $project) }}">
                {{ $project->title }}
            </a>
            {{-- basket and gift buttons --}}
            <div class="baskets flex">
                @if ($project->forDonate())
                <div class="amount-basket bas-left">
                    <button class="btn @if (!$project->isDone()) add-to-cart @endif" href="javascript:void(0)"
                        data-project-id="{{ $project->id }}" x-bind:data-price="totalPrice"
                        x-bind:data-amount="increment" style="width:40px">
                        <img src="{{ asset('/assets/img/add_cart.svg') }}">
                    </button>
                </div>
                @endif
                @if ($project->is_giftable)
                    <div class="amount-basket">
                        <button class="btn @if (!$project->isDone()) add-gift-to-cart @endif"
                            href="javascript:void(0)" data-toggle="modal" data-target=".login-popup"
                            data-project-id="{{ $project->id }}" x-bind:data-price="totalPrice"
                            x-bind:data-amount="increment" style="width:40px">
                            <img src="{{ asset('/assets/img/gift.svg') }}">
                        </button>
                    </div>
                @endif
            </div>
        </h5>
        <p class="h1 mb0 mt2 gray f6 truncate-ns lh-solid">
        <div class="showdesc m-0">
            @if ($long_description)
                {!! $project->description !!}
            @else
                {!! $project->short_description !!}
                <a href="{{ route('projects.show', $project) }}"
                    style="cursor:pointer; text-decoration:underline !important;" class="clr-primary">
                    ...
                </a>
            @endif
        </div>
        </p>
        @if ($project->forDonate())
            {{-- donation amount and progress bar  --}}
            <div class="mt3 flex items-center justify-between">
                <div class="input-group increment-group flex-nowrap">
                    {{-- (+) --}}
                    <span class="input-group-append">
                        <button type="button" class="inc-dec-btn"
                            @if (!$project->isDone()) x-on:click="increment++;calculateTotalPrice()" @endif>
                            <span class="fa fa-plus"></span>
                        </button>
                    </span>
                    <input type="number" class="text-dark add-am-bt p-donate-num" id="famount" name="famount"
                        x-model="totalPrice" @change="price=event.target.value;increment=1" min="1"
                        placeholder="أضف المبلغ">
                    {{-- (-) --}}
                    <span class="input-group-prepend">
                        <button type="button" class="inc-dec-btn"
                            @if (!$project->isDone()) x-on:click="(increment==1)? 1 : increment--;calculateTotalPrice()" @endif>
                            <span class="fa fa-minus"></span>
                        </button>
                    </span>
                </div>
                {{-- progress bar --}}
                @if ($project->is_goalable)
                    <div class="flex items-center lato br-pill flex-auto flex-grow-0 style-module--wrapper--3NhEB"><span
                            class="ph3-ns ph2 f6 nowrap truncate black rtl">{{ $project->goal }} ر.س</span>
                        <div class="relative flex items-center brand br-pill style-module--progress--38OK7"><span
                                class="w-100 tc absolute center black f7 fw5">{{ $project->complete_percent }}%</span>
                            <svg height="48" viewBox="0 0 36 36">
                                <path stroke-dasharray="{{ $project->complete_percent }}, 100" stroke="currentColor"
                                    stroke-width="4" stroke-linecap="round" fill="none"
                                    d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831">
                                </path>
                            </svg>
                        </div>
                    </div>
                @endif
            </div>
            {{-- الاسهم --}}
            @if (count($project->shares) > 0)
                <div class="mt-4 flex items-center justify-between amount-last align-items-start">
                    <div class="d-amount-price">
                        @foreach ($project->shares as $share)
                            <button class="btn share"
                                x-on:click="increment=1;price={{ $share['price'] }};calculateTotalPrice()">
                                {{ $share['title'] }}
                            </button>
                        @endforeach
                    </div>
                </div>
            @endif
        @endif
    </div>
</div>
