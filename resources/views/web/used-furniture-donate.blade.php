@extends('web.layouts.app')
@section('content')
    <div data-ng-app="myApp" data-ng-controller="myCtrl">
        <section class="header projects-header flex justify-center align-center text-center">
            <div class="container">
                <h5>
                    تواصل معنا
                </h5>
            </div>
        </section>
        <section class="header">
            <nav aria-label="breadcrumb" class="breadcrumb-bg">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('home')}}">الرئيسية</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">تبرع</li>
                    </ol>
                </div>
            </nav>
        </section>


        <div class="main-page">
            <section class="contact-page-main">
                <div class="container">

                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="contact-inr">
                                @if(session('success'))
                                    <div class="alert alert-success"
                                         role="alert">
                                        {{session('success')}}
                                    </div>
                                @endif
                                <div class="sec-title">
                                    <h2>التبرع بالأثاث/الأجهزة الكهربائية</h2>
                                </div>
                                <div class="contact-fo">
                                    <form method="post" action="{{route('send-used-furniture-donation')}}"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <input type="text" name="donor_name" class="form-control" required
                                                           placeholder="الاسم">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <input type="text" name="donor_mobile" class="form-control" required
                                                           placeholder="رقم الجوال">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <input type="email" name="email" class="form-control" required
                                                           placeholder="الايميل">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <input type="text" name="city" class="form-control" required
                                                           placeholder="المدينة">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <input type="text" name="neighborhood" class="form-control" required
                                                           placeholder="الحي">
                                                </div>
                                            </div>
                                            @if($furniture_types = tenant()->user?->store?->furniture_types)
                                                <div class="col-md-6">
                                                    <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                        <select class="form-control w-100 fo-input" name="type"
                                                                required>
                                                            <option disabled>النوع</option>
                                                            @foreach($furniture_types as $furniture_type)
                                                                <option>{{$furniture_type['type']}}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="select-arrow">
                                                            <i class="fas fa-arrow-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="col-md-6">
                                                    <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                        <select class="form-control w-100 fo-input" name="type"
                                                                required>
                                                            <option disabled>النوع</option>
                                                            <option>أثاث</option>
                                                            <option>أجهزة كهربائية</option>
                                                        </select>
                                                        <div class="select-arrow">
                                                            <i class="fas fa-arrow-down"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-md-6">
                                                <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <select class="form-control w-100 fo-input" name="condition"
                                                            required>
                                                        <option disabled>الحالة</option>
                                                        <option>جديد</option>
                                                        <option>مستعمل</option>
                                                    </select>
                                                    <div class="select-arrow">
                                                        <i class="fas fa-arrow-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <input type="number" name="quantity" class="form-control" required
                                                           dir="rtl" style="direction: rtl"
                                                           placeholder="الكمية">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <input type="file" name="images[]" class="form-control" required
                                                           multiple accept="image/*"
                                                           placeholder="الصور">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">

                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <button type="submit" class="btn fo-bt">إرسال</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
@endsection

