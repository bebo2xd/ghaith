@extends('web.layouts.app')
@section('content')
    <div class="main-page home-page">
        <x-breadcrumb>
            @if(isset($category_title))
                <li class="breadcrumb-item " aria-current="page">الأقسام</li>
                <li class="breadcrumb-item active" aria-current="page">{{$category_title}}</li>
            @else
                <li class="breadcrumb-item active" aria-current="page">المشاريع</li>
            @endif
        </x-breadcrumb>
        <livewire:project/>
    </div>
    @include('web.modals.share')
@endsection
