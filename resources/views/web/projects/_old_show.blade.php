@extends('web.layouts.app')
@section('content')
    <style>
        @keyframes growProgressBar {
            0%, 33% {
                --pgPercentage: 0;
            }
            100% {
                --pgPercentage: var(--value);
            }
        }

        @property --pgPercentage {
            syntax: '<number>';
            inherits: false;
            initial-value: 0;
        }

        div[role="progressbar"] {
            --size: 5rem;
            --fg: rgb(81,178,196);
            --bg: #def;
            --pgPercentage: var(--value);
            animation: growProgressBar 3s 1 forwards;
            width: var(--size);
            height: var(--size);
            border-radius: 50%;
            display: grid;
            place-items: center;
            background: radial-gradient(closest-side, white 80%, transparent 0 99.9%, white 0),
            conic-gradient(var(--fg) calc(var(--pgPercentage) * 1%), var(--bg) 0);
            font-family: Helvetica, Arial, sans-serif;
            font-size: calc(var(--size) / 5);
            color: var(--fg);
        }

        div[role="progressbar"]::before {
            counter-reset: percentage var(--value);
            content: counter(percentage) '%';
        }

    </style>
    <div class="main-page home-page">
        <x-breadcrumb>
            <li class="breadcrumb-item " aria-current="page">المشاريع</li>
            <li class="breadcrumb-item active" aria-current="page">{{$project->title}}</li>
        </x-breadcrumb>
        <div class="container">
            <x-project :project="$project" :long_description="true"></x-project>
        </div>
    </div>
    @include('web.modals.share')
@endsection
