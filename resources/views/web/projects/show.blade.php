@extends('web.layouts.app')
@section('content')
    <style>
        /*! CSS Used from: https://abrarkw.org/css/front.css */
        li {
            list-style: none;
        }

        h1 {
            font-size: 2em;
            margin: .67em 0;
        }

        button,
        input {
            font-size: 100%;
            line-height: 1.15;
            margin: 0;
        }

        button,
        input {
            overflow: visible;
        }

        button {
            text-transform: none;
        }

        [type=button],
        button {
            -webkit-appearance: button;
        }

        div,
        h1,
        h5,
        li,
        p {
            box-sizing: border-box;
        }

        .container {
            /* overflow-x: hidden; */
        }

        .cover {
            background-size: cover !important;
            border-radius: 15px;
        }

        .bg-center {
            background-position: 50%;
        }

        .bg-center {
            background-repeat: no-repeat;
        }

        .ba {
            border-style: solid;
            border-width: 1px;
        }

        .bb {
            border-bottom-style: solid;
            border-bottom-width: 1px;
        }

        .bn {
            border-style: none;
            border-width: 0;
        }

        .b--light-gray {
            border-color: #eee;
        }

        .b--black-05 {
            border-color: rgba(0, 0, 0, .05);
        }

        .br2 {
            border-radius: .25rem;
        }

        .br-pill {
            border-radius: 9999px;
        }

        .bw1 {
            border-width: .125rem;
        }

        .left-0 {
            left: 0;
        }

        .absolute--fill {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .db {
            display: block;
        }

        .dib {
            display: inline-block;
        }

        .flex {
            display: flex;
        }

        .flex-auto {
            flex: 1 1 auto;
            min-width: 0;
            min-height: 0;
        }

        .flex-column {
            flex-direction: column;
        }

        .items-start {
            align-items: flex-start;
        }

        .items-center {
            align-items: center;
        }

        .justify-center {
            justify-content: center;
        }

        .justify-between {
            justify-content: space-between;
        }

        .flex-shrink-0 {
            flex-shrink: 0;
        }

        .fw6 {
            font-weight: 600;
        }

        .h-100 {
            height: 100%;
        }

        .lh-solid {
            line-height: 1;
        }

        .lh-copy {
            line-height: 1.5;
        }

        .w-100 {
            width: 100%;
        }

        .relative {
            position: relative;
        }

        .absolute {
            position: absolute;
        }

        .black {
            color: #000;
        }

        .white {
            color: #fff;
        }

        .bg-black-05 {
            background-color: rgba(0, 0, 0, .05);
        }

        .bg-white {
            background-color: #fff;
        }

        .bg-transparent {
            background-color: transparent;
        }

        .pa2 {
            padding: .5rem;
        }

        .pb3 {
            padding-bottom: 1rem;
        }

        .pv3 {
            padding-top: 1rem;
            padding-bottom: 1rem;
        }

        .pv5 {
            padding-top: 4rem;
            padding-bottom: 4rem;
        }

        .ph3 {
            padding-left: 1rem;
            padding-right: 1rem;
        }

        .ph4 {
            padding-left: 0.5rem;
            padding-right: 0.5rem;
        }

        .mr3 {
            margin-right: 1rem;
        }

        .mb2 {
            margin-bottom: .5rem !important;
        }

        .mt0 {
            margin-top: 0;
        }

        .mt2 {
            margin-top: .5rem;
        }

        .mt4 {
            margin-top: 2rem;
        }

        .mv0 {
            margin-top: 0;
            margin-bottom: 0;
        }

        .mv2 {
            margin-top: .5rem;
            margin-bottom: .5rem;
        }

        .mv3 {
            margin-top: 1rem;
            margin-bottom: 1rem;
        }

        .mh3 {
            margin-left: 1rem;
            margin-right: 1rem;
        }

        .nl3 {
            margin-left: -1rem;
        }

        .nr3 {
            margin-right: -1rem;
        }

        .nt2 {
            margin-top: -.5rem;
        }

        .f4 {
            font-size: 1.25rem;
        }

        .f5 {
            font-size: 1rem;
        }

        .f6 {
            font-size: .875rem;
        }

        .center {
            margin-left: auto;
        }

        .center {
            margin-right: auto;
        }

        .nowrap {
            white-space: nowrap;
        }

        .dim {
            opacity: 1;
        }

        .dim,
        .dim:focus,
        .dim:hover {
            transition: opacity .15s ease-in;
        }

        .dim:focus,
        .dim:hover {
            opacity: .5;
        }

        .dim:active {
            opacity: .8;
            transition: opacity .15s ease-out;
        }

        .grow {
            -moz-osx-font-smoothing: grayscale;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            transform: translateZ(0);
            transition: transform .25s ease-out;
        }

        .grow:focus,
        .grow:hover {
            transform: scale(1.05);
        }

        .grow:active {
            transform: scale(.9);
        }

        .pointer:hover {
            cursor: pointer;
        }

        .brand {
            color: var(--primary);
        }

        .bg-brand {
            background-color: var(--primary);
        }

        button,
        input {
            font-family: Source Sans Pro, sans-serif;
        }

        button,
        input {
            outline: none;
        }

        .lato {
            font-family: Lato, sans-serif;
            direction: ltr;
        }

        button,
        input {
            font-family: inherit;
            text-align: initial;
        }

        @media screen and (max-width:960px) {
            .ph4 {
                padding-left: 0.25rem;
                padding-right: 0.25rem;
            }

            .baskets.flex {
                flex-direction: column;
                margin: 0 1rem !important;
            }

            .baskets.flex button {
                text-align: center;
            }
        }

        @media screen and (min-width: 960px) {
            .flex-ns {
                display: flex;
            }

            .ph4-ns {
                padding-left: 2rem;
                padding-right: 2rem;
            }

            .f5-ns {
                font-size: 1rem;
            }
        }

        /*! CSS Used from: https://abrarkw.org/css/app.c1f8ebc3.css */
        .pr3 {
            padding-right: 2rem;
        }

        h1 {
            font-size: 2em;
            margin: .67em 0;
        }

        button,
        input {
            font-size: 100%;
            line-height: 1.15;
            margin: 0;
            overflow: visible;
        }

        button {
            text-transform: none;
        }

        [type=button],
        button {
            -webkit-appearance: button;
        }

        div,
        h1,
        h5,
        p {
            box-sizing: border-box;
        }

        .cover {
            background-size: cover !important;
        }

        .bg-center {
            background-position: 50%;
            background-repeat: no-repeat;
        }

        .ba {
            border-style: solid;
            border-width: 1px;
        }

        .bb {
            border-bottom-style: solid;
            border-bottom-width: 1px;
        }

        .bn {
            border-style: none;
            border-width: 0;
        }

        .b--light-gray {
            border-color: #eee;
        }

        .b--black-05 {
            border-color: rgba(0, 0, 0, .05);
        }

        .br2 {
            border-radius: .25rem;
        }

        .br-pill {
            border-radius: 9999px;
        }

        .bw1 {
            border-width: .125rem;
        }

        .top-0 {
            top: 0;
        }

        .absolute--fill {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .cf:after,
        .cf:before {
            content: " ";
            display: table;
        }

        .cf:after {
            clear: both;
        }

        .cf {
            *zoom: 1;
        }

        .db {
            display: block;
        }

        .dib {
            display: inline-block;
        }

        .flex {
            display: flex;
        }

        .flex-auto {
            flex: 1 1 auto;
            min-width: 0;
            min-height: 0;
        }

        .flex-column {
            flex-direction: column;
        }

        .flex-row {
            flex-direction: row;
        }

        .items-start {
            align-items: flex-start;
        }

        .items-center {
            align-items: center;
        }

        .items-stretch {
            align-items: stretch;
        }

        .justify-center {
            justify-content: center;
        }

        .justify-between {
            justify-content: space-between;
        }

        .flex-shrink-0 {
            flex-shrink: 0;
        }

        .fl {
            float: left;
            _display: inline;
        }

        .fw6 {
            font-weight: 600;
        }

        .input-reset {
            -webkit-appearance: none;
            -moz-appearance: none;
        }

        .h-100 {
            height: 100%;
        }

        .h-auto {
            height: auto;
        }

        .lh-solid {
            line-height: 1;
        }

        .lh-copy {
            line-height: 1.5;
        }

        .w-100 {
            width: 100%;
        }

        .relative {
            position: relative;
        }

        .absolute {
            position: absolute;
        }

        .o-50 {
            opacity: .5;
        }

        .black {
            color: #000;
        }

        .white {
            color: #fff;
        }

        .color-inherit {
            color: inherit;
        }

        .bg-black-05 {
            background-color: rgba(0, 0, 0, .05);
        }

        .bg-white {
            background-color: #fff;
        }

        .bg-transparent {
            background-color: transparent;
        }

        .pa2 {
            padding: .5rem;
        }

        .pl3 {
            padding-left: 1rem;
        }

        .pb3 {
            padding-bottom: 1rem;
        }

        .pv1 {
            padding-top: .25rem;
            padding-bottom: .25rem;
        }

        .pv3 {
            padding-top: 1rem;
            padding-bottom: 1rem;
        }

        .ph3 {
            padding-left: 1rem;
            padding-right: 1rem;
        }

        .ph4 {
            padding-left: 2rem;
            padding-right: 2rem;
        }

        .ma1 {
            margin: .25rem;
        }

        .mr3 {
            margin-right: 1rem;
        }

        .mr4 {
            margin-right: 2rem;
        }

        .mb2 {
            margin-bottom: .5rem;
        }

        .mb4 {
            margin-bottom: 2rem;
        }

        .mt0 {
            margin-top: 0;
        }

        .mt2 {
            margin-top: .5rem;
        }

        .mt4 {
            margin-top: 2rem;
        }

        .mv0 {
            margin-top: 0;
            margin-bottom: 0;
        }

        .mv2 {
            margin-top: .5rem;
            margin-bottom: .5rem;
        }

        .mv3 {
            margin-top: 1rem;
            margin-bottom: 1rem;
        }

        .mh3 {
            margin-left: 1rem;
            margin-right: 1rem;
        }

        .na1 {
            margin: -.25rem;
        }

        .nl3 {
            margin-left: -1rem;
        }

        .nr3 {
            margin-right: -1rem;
        }

        .nb2 {
            margin-bottom: -.5rem;
        }

        .nt2 {
            margin-top: -.5rem;
        }

        .tl {
            text-align: left;
        }

        .f4 {
            font-size: 1.25rem;
        }

        .f5 {
            font-size: 1rem;
        }

        .f6 {
            font-size: .875rem;
        }

        .nowrap {
            white-space: nowrap;
        }

        .dim {
            opacity: 1;
        }

        .dim,
        .dim:focus,
        .dim:hover {
            transition: opacity .15s ease-in;
        }

        .dim:focus,
        .dim:hover {
            opacity: .5;
        }

        .dim:active {
            opacity: .8;
            transition: opacity .15s ease-out;
        }

        .grow {
            -moz-osx-font-smoothing: grayscale;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            transform: translateZ(0);
            transition: transform .25s ease-out;
        }

        .grow:focus,
        .grow:hover {
            transform: scale(1.05);
        }

        .grow:active {
            transform: scale(.9);
        }

        .pointer:hover {
            cursor: pointer;
        }

        @media screen and (min-width:30em) {
            .flex-ns {
                display: flex;
            }

            .w-50-ns {
                width: 50%;
            }

            .pa3-ns {
                padding: 1rem;
            }

            .mb0-ns {
                margin-bottom: 0;
            }

            .f3-ns {
                font-size: 1.5rem;
            }

            .f5-ns {
                font-size: 1rem;
            }
        }

        .custom-gray {
            color: #3bac07;
        }

        button,
        input {
            outline: none;
        }

        .lato {
            font-family: Noto Kufi Arabic, sans-serif;
            direction: rtl;
        }

        .quick-donate {
            display: flex;
            flex-wrap: wrap;
        }

        .quick-donate button {
            flex: 1 0 23%;
            text-align: center;
        }

        /*! CSS Used fontfaces */
        @font-face {
            font-family: 'Noto Kufi Arabic';
            font-style: normal;
            font-weight: 400;
            src: url(https://abrarkw.org/fonts/noto-kufi-400.woff2) format('woff2');
            unicode-range: U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC, U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        .pcatshare {
            background-color: #fef2de !important;
            padding: 5px 20px !important;
            border-radius: 20px !important;
        }
    </style>


    <div class="main-page home-page">
        <x-breadcrumb>
            <li class="breadcrumb-item " aria-current="page">المشاريع</li>
            <li class="breadcrumb-item active" aria-current="page">{{ $project->title }}</li>
        </x-breadcrumb>


        <div class="container">
            <div class="bg-custom-light-gray">
                <div class="center text-start">
                    <div class="mb4 flex items-start justify-center" style="border-color: rgb(219, 224, 229);">
                        <h1 class="mv0 black f3-ns f4">{{ $project->title }}</h1>
                    </div>


                    <div class="flex-ns justify-between">
                        <div class="w-50-ns flex-shrink-0">
                            <div class="relative cover bg-center mb0-ns mb4"
                                style="padding-top: 55%; background-image: url({{ $project->logo_url }});">
                                <div class="absolute absolute--fill bg-black-05 ba b--black-05">
                                </div>
                            </div>
                        </div>
                        <div class="mr4"></div>

                        {{-- project data --}}
                        <div class="flex-auto flex flex-column" x-data="{
                            increment: 1,
                            price: 1,
                            totalPrice: 1,
                            calculateTotalPrice() { this.totalPrice = this.price * this.increment }
                        }">
                            {{-- progress bar --}}
                            @if ($project->is_goalable)
                                <div class="lh-solid lato mb4">
                                    <h5 class="mt0 mb2 f4">{{ $project->complete_percent }}%</h5>
                                    <div class="relative">
                                        <div class="bg-brand o-50 br-pill" style="height: 12px;"></div>
                                        <div class="absolute h-100 top-0 bg-brand br-pill"
                                            style="width: {{ $project->complete_percent }}%;"></div>
                                    </div>
                                    <div class="mt2 flex items-center justify-between f5">
                                        <span class="mh3">0 ر.س</span>
                                        <span class="flex-auto">
                                            <div class="tl brand nowrap" style="width: {{ $project->complete_percent }}%;">
                                                <span class="dib">{{ $project->goal - $project->remaining }} ر.س</span>
                                            </div>
                                        </span>
                                        <span class="mh3">{{ $project->goal }} ر.س</span>
                                    </div>
                                </div>
                            @endif
                            @if ($project->forDonate())
                                <div class="mb4 flex-auto flex flex-row items-center" style="">
                                    <div class="fl" style="font-size: 16px;">
                                        <p class="mt0 mb2"><span>ادخل تبرعك:</span>
                                        </p>
                                        <div class="mv3 lh-solid">
                                            <div class="na1 quick-donate d-amount-price">
                                                @foreach ($quickValues = [5, 10, 20, 50, 100, 200, 500] as $v)
                                                    <button type="button" class="btn share me-2"
                                                        x-on:click="increment=1;price={{ $v }};calculateTotalPrice()">
                                                        <span>{{ $v }} ر.س</span>
                                                    </button>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="flex items-stretch lh-solid ">
                                            @if ($project->forDonate())
                                                <div class="me-5 flex">
                                                    {{-- (+) --}}
                                                    <span class="input-group-append">
                                                        <button type="button" class="inc-dec-btn"
                                                            @if (!$project->isDone()) x-on:click="increment++;calculateTotalPrice()" @endif>
                                                            <span class="fa fa-plus"></span>
                                                        </button>
                                                    </span>
                                                    {{-- input --}}
                                                    <div class="relative flex items-center flex-auto lh-solid mx-2"><span
                                                            class="absolute left-0 ph3">ر.س</span>
                                                        <input type="number" placeholder="مبلغ آخر" id="famount"
                                                            name="famount" x-model="totalPrice"
                                                            @change="price=event.target.value;increment=1" min="1"
                                                            class="w-100 text-dark add-am-bt p-donate-num">
                                                    </div>
                                                    {{-- (-) --}}
                                                    <span class="input-group-prepend">
                                                        <button type="button" class="inc-dec-btn"
                                                            @if (!$project->isDone()) x-on:click="(increment==1)? 1 : increment--;calculateTotalPrice()" @endif>
                                                            <span class="fa fa-minus"></span>
                                                        </button>
                                                    </span>
                                                </div>
                                            @endif

                                            @if (!$project->isDone())
                                                <div class="baskets flex justify-between gap-1">
                                                    @if ($project->forDonate())
                                                        <button class="pv3 ph4 bg-brand white fw6 rounded-pill add-to-cart"
                                                            href="javascript:void(0)" data-project-id="{{ $project->id }}"
                                                            x-bind:data-price="totalPrice" x-bind:data-amount="increment">
                                                            تبرع
                                                        </button>
                                                    @endif
                                                    @if ($project->is_giftable)
                                                        <div class="amount-basket">
                                                            <button
                                                                class="pv3 ph4 bg-brand white fw6 rounded-pill add-gift-to-cart"
                                                                href="javascript:void(0)" data-toggle="modal"
                                                                data-target=".login-popup"
                                                                data-project-id="{{ $project->id }}"
                                                                x-bind:data-price="totalPrice"
                                                                x-bind:data-amount="increment">
                                                                إهداء
                                                            </button>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>

                            @elseif (!$project->forDonate() && $project->is_giftable)
                            <div class="amount-basket text-center my-3">
                                <button
                                    class="py-2 ph4 bg-brand white fw6 rounded-pill add-gift-to-cart text-center"
                                    href="javascript:void(0)" data-toggle="modal"
                                    data-target=".login-popup"
                                    data-project-id="{{ $project->id }}"
                                    x-bind:data-price="totalPrice"
                                    x-bind:data-amount="increment">
                                    إهداء
                                </button>
                            </div>
                            @endif
                            <div class="nt2 nb2">
                                <p class="mv2 lh-copy">{!! $project->description !!}</p>
                            </div>
                            <div class="mt4">
                                <div class="flex justify-between pb-2" style=" border-bottom: 2px solid var(--primary);">
                                    <div class="f5">
                                        <b>التصنيف: </b>
                                        <a href="{{ route('project-category.index', $project->project_category_id) }}"
                                            class="btn pcatshare clr-primary">{{ $project->category->title }}</a>
                                    </div>
                                    <div class="f6">
                                        {{-- <a href="#">مشاركة المشروع <span class="fa fa-share"></span></a> --}}
                                        <span class="btn pcatshare clr-primary" style="cursor: pointer"
                                            onclick="showShareURL('{{ route('projects.show', $project) }}')">
                                            مشاركة المشروع <span class="fa fa-share"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            @if ($project->is_goalable)
                                <div class="flex justify-between mt-2 mb-5">
                                    <div class="f6"> <b>هدف المشروع: </b> {{ $project->goal }} ر.س</div>
                                    <div class="f6"> <b>المتبقي: </b> {{ $project->remaining ?: '0' }} ر.س</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('web.modals.share')
@endsection
