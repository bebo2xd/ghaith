{{-- <style>
    @keyframes growProgressBar {
        0%, 33% {
            --pgPercentage: 0;
        }
        100% {
            --pgPercentage: var(--value);
        }
    }

    @property --pgPercentage {
        syntax: '<number>';
        inherits: false;
        initial-value: 0;
    }

    div[role="progressbar"] {
        --size: 5rem;
        --fg: rgb(81,178,196);
        --bg: #def;
        --pgPercentage: var(--value);
        animation: growProgressBar 3s 1 forwards;
        width: var(--size);
        height: var(--size);
        border-radius: 50%;
        display: grid;
        place-items: center;
        background: radial-gradient(closest-side, white 80%, transparent 0 99.9%, white 0),
        conic-gradient(var(--fg) calc(var(--pgPercentage) * 1%), var(--bg) 0);
        font-family: Helvetica, Arial, sans-serif;
        font-size: calc(var(--size) / 5);
        color: var(--fg);
    }

    div[role="progressbar"]::before {
        counter-reset: percentage var(--value);
        content: counter(percentage) '%';
    }

</style> --}}
<div class="main-page">
    <section class="project-main">
        <div class="container">
            <div class="row">
                @foreach ($projects as $project)
                    <div class="col-lg-4" style="margin-bottom: 1rem">
                        <x-project :project="$project"></x-project>
                    </div>
                @endforeach
                @if(method_exists($projects,'links'))
                    {{$projects->links()}}
                @else
                    <div class="categories-bt text-center mb-3">
                        <a class="btn" href="{{route('projects.index')}}">
                            عرض المشاريع
                        </a>
                    </div>
                @endif
            </div>
            {{-- <div class="row">
                <div class="block">
                    مشاريع التبرع
                </div>
                @foreach ($donationProjects??[] as $project)
                    <div class="col-lg-4" style="margin-bottom: 1rem">
                        <x-project :project="$project"></x-project>
                    </div>
                @endforeach
                @if(method_exists($projects,'links'))
                    {{$projects->links()}}
                @else
                    <div class="categories-bt text-center mb-3">
                        <a class="btn" href="{{route('projects.index')}}">
                            عرض المشاريع
                        </a>
                    </div>
                @endif
            </div> --}}
        </div>
    </section>
</div>
