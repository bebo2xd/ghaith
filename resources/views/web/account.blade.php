@extends('web.layouts.app')
@section('content')

    <!-- new design Account-->
    <section class="header ">
        <nav aria-label="breadcrumb" class="breadcrumb-bg">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">الرئيسية</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">الملف الشخصي</li>
                </ol>
            </div>
        </nav>
    </section>

    <div class="main-page">
        <section class="profile-main">
            <div class="container">
                <div class="row justify-center">
                    <div class="col-md-10 col-lg-8">
                        @if(session('success'))
                            <div class="alert alert-success"
                                 role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        <div class="proflie-inr">
                            <div class="profile-title text-center mb-3">
                                <h2>الملف الشخصي</h2>
                            </div>
                            <div class="profile-info">
                                <nav class="container">
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-link active" id="nav-public-tab" data-toggle="tab"
                                           href="#nav-public" role="tab" aria-controls="nav-public"
                                           aria-selected="true">الكل
                                        </a>
                                        <a class="nav-link" id="nav-donation-tab" data-toggle="tab" href="#nav-donation"
                                           role="tab" aria-controls="nav-donation" aria-selected="false">
                                            التبرعات
                                        </a>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-public" role="tabpanel"
                                         aria-labelledby="nav-public-tab">
                                        <div class="profile-info-img">
                                            <img src="{{asset('/assets/img/user.svg')}}">
                                            <h2>{{$user->name}}</h2>
                                        </div>
                                        <div class="profile-details text-right">
                                            <form action="{{ route('update-profile')}}" method="POST" id="profile">
                                                @csrf
                                                <input type="hidden" id="user_id" value="{{$user->id}}"
                                                       name="user_id">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group profile-info-fo">
                                                            <label class="gr-top-label fo-input-head">الاسم</label>
                                                            <input type="text" class="form-control fo-input"
                                                                   name="name"
                                                                   placeholder="" value="{{$user->name}}">
                                                            @error('name')
                                                            <label for="" class="text-red-400">{{$message}}</label>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group profile-info-fo">
                                                            <label class="gr-top-label fo-input-head">البريد </label>
                                                            <input type="text" class="form-control fo-input"
                                                                   name="email" value="{{$user->email}}"
                                                                   placeholder="example@gmail.com">
                                                            @error('email')
                                                            <label for="" class="text-red-400">{{$message}}</label>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group profile-info-fo">
                                                            <label class="gr-top-label fo-input-head">رقم </label>
                                                            <input type="text" class="form-control fo-input"
                                                                   name="phone" placeholder="0501410202"
                                                                   value="{{$user->phone}}">
                                                            @error('phone')
                                                            <label for="" class="text-red-400">{{$message}}</label>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="profile-info-bt">
                                                    <button type="submit" name="submit" form="profile" class="btn">حفظ
                                                    </button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade show " id="nav-donation" role="tabpanel"
                                         aria-labelledby="nav-donation-tab">
                                        <div class="profile-info-img">
                                            <h2>سجل </h2>
                                        </div>
                                        <div class="reg-details text-start">
                                            @foreach($orders as $order )
                                                <div class="reg-details-inr">
                                                    <div class="reg-pro-info">
                                                        <h2>المشروع</h2>
                                                        <span>{{ $order->orderName() }}</span>
                                                    </div>
                                                    <div class="reg-pro-num text-center">
                                                        <h2>العدد</h2>
                                                        <span>{{ $order->orderCount() }}</span>
                                                    </div>
                                                    <div class="reg-price-info text-center">
                                                        <h2>المبلغ</h2>
                                                        <span>{{ $order->amount }} ر.س</span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- new design Account-->

    <section class="header account-header flex justify-center align-center text-center">
        <div class="container">
            <h5>
                إدارة حسابي
            </h5>
        </div>
    </section>

    <script>
        // $.noConflict();
        $(document).ready(function () {
            $("#tab-donation2").click(function () {
                $(".donationselect").addClass("cd-tabs__item--selected");
                $(".donationselectpanel").addClass("cd-tabs__panel--selected");

                $(".accountselect").removeClass("cd-tabs__item--selected");
                $(".accountselectpanel").removeClass("cd-tabs__panel--selected");
            });
        });

    </script>

@endsection
