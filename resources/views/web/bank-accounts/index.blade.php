@extends('web.layouts.app')
@section('content')

    <div class="main-page home-page">
        <section class="acc-page-main">
            <div class="container">
                <div class="row">
                    @foreach ($banks as $bank)
                        <div class="col-lg-3 col-md-6">
                            <div class="acc-box">
                                <div class="acc-img">
                                    <img class="img-fluid project-img"
                                         src="{{ $bank->logo_url }}"/>
                                </div>
                                <div class="acc-img-info">
                                    <h3>{{ $bank->title }}</h3>
                                    <span> {{ $bank->iban }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{ $banks->links() }}
                </div>
            </div>
        </section>
    </div>
@endsection
