@extends('web.layouts.app')
@section('content')
    <div data-ng-app="myApp" data-ng-controller="myCtrl">
        <section class="header projects-header flex justify-center align-center text-center">
            <div class="container">
                <h5>
                    تواصل معنا
                </h5>
            </div>
        </section>
        <section class="header">
            <nav aria-label="breadcrumb" class="breadcrumb-bg">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('home') }}">الرئيسية</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">تواصل معنا</li>
                    </ol>
                </div>
            </nav>
        </section>


        <div class="main-page">
            <section class="contact-page-main">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="text-start">
                                @if (session('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('success') }}
                                    </div>
                                @endif
                                <div class="sec-title">
                                    <h2>تواصل معنا</h2>
                                </div>
                                <div class="contact-fo">
                                    <h3>تفضل بإرسال استفسارك من خلال النموذج التالي</h3>
                                    <form method="post" action="{{ route('contact-us') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <input type="text" name="name" class="form-control" required
                                                        placeholder="الاسم">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <input type="text" name="email" class="form-control" required
                                                        placeholder="البريد الإلكتروني">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <input type="text" name="subject" class="form-control" required
                                                        placeholder="الموضوع">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div
                                                    class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <select class="form-control w-100 fo-input" name="message_type" required
                                                        id="#">
                                                        <option disabled>نوع الرسالة</option>
                                                        <option>عام</option>
                                                        <option>استفسار</option>
                                                        <option>شكوى</option>
                                                    </select>
                                                    <div class="select-arrow">
                                                        <i class="fas fa-arrow-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <textarea class="form-control" name="message" rows="3" required placeholder="أكتب رسالتك هنا"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                                    <button type="submit" class="btn fo-bt">إرسال</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 text-start">
                            <div class="sec-title">
                                <h2>طرق التواصل</h2>
                            </div>
                            <div class="mb-lg-4 mb-2">
                                <a class="h5 clr-primary">العنوان</a>
                                <div>{{ tenant_store('location') }}</div>
                            </div>
                            <div class="mb-lg-4 mb-2">
                                <a class="h5 clr-primary">البريد
                                    الالكتروني </a>
                                <div class="mt-2">{{ tenant_store('contact_email') }}</div>
                            </div>
                            <div class="mb-lg-4 mb-2">
                                <a class="h5 clr-primary">التليفون</a>
                                <div class="mt-2">{{ tenant_store('mobile') }}</div>
                            </div>
                            <div class="mb-lg-4 mb-2">
                                <a class="h5 clr-primary">الواتس اب</a>
                                <div class="mt-2">{{ tenant_store('whatsapp') }}</div>
                            </div>
                        </div>
                    </div>

                    @if (tenant_store('google_maps'))
                    <div class="mt-5">
                        {{-- <iframe
                            src=""
                            width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                            referrerpolicy="no-referrer-when-downgrade"></iframe> --}}
                        <iframe src="{{tenant_store('google_maps')}}" width="100%" height="320px" loading="lazy"
                            style="border: none;"></iframe>
                    </div>
                    @endif
                </div>
            </section>
        </div>
    @endsection
