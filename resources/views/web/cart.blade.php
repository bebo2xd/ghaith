@extends('web.layouts.app')
@section('content')

    <section class="header cart-header flex justify-center align-center text-center">
        <div class="container">
            <h5>
                سلة التبرعات
            </h5>
        </div>
    </section>

    <!-- cart  -->
    <section class="header ">
        <nav aria-label="breadcrumb" class="breadcrumb-bg">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">الرئيسية</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">السلة</li>
                </ol>
                @if(session('response'))
                    <div class="alert text-center @if(session('response')['status']==true) alert-success @else alert-danger @endif"
                         role="alert">
                        {{session('response')['message']}}
                    </div>
                @endif
            </div>
        </nav>
        <div class="container">

        </div>
    </section>


    <div class="main-page">
        <section class="cart-main">
            <div class="container">
                <div class="row justify-center">
                    <div class="col-md-10">
                        <div class="cart-inr">
                            <div class="cart-title text-center">
                                <h2>السلة</h2>
                            </div>
                            <div class="row">
                                <div class="cart-inr-right">
                                    @forelse ($cart as $item)
                                        <div class="alert alert-warning alert-dismissible fade show"
                                             role="alert">
                                            <div class="cart-right-inr">
                                                <div class="row">
                                                    @if($item->project)
                                                        <div class="col-md-5">
                                                            <div class="cart-right-img">
                                                                <img src="{{ $item->project->logo_url }}" width="100"
                                                                     height="100"
                                                                     class="img-responsive rounded-circle"/>
                                                                <div class="cart-img-info">
                                                                    <h3>المشروع</h3>
                                                                    <p>{{ $item->project->title }}</p>
                                                                    @if ($item->options->name)
                                                                        <p>هدية ل
                                                                           : {{ $item->options->name }}</p>
                                                                        <p>{{ $item->options->mobile }}</p>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="col-md-5">
                                                            <div class="cart-right-img">
                                                                <img src="" width="100"
                                                                     height="100"
                                                                     class="img-responsive rounded-circle"/>
                                                                <div class="cart-img-info">
                                                                    <h3>تبرع سريع</h3>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="col-md-3 col-6">
                                                        <div class="cart-counter">
                                                            <div class="cart-counter-title">
                                                                <h2>العدد</h2>
                                                            </div>
                                                            <div class="cart-counter-inr">
                                                                <div class="number-input text-center">
                                                                    {{ $item->qty }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-6">
                                                        <div class="cart-price text-center">
                                                            <h2>السعر</h2>
                                                            <span data-th="Subtotal">{{ $item->price }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 d-flex align-items-center">
                                                        <button type="button" class="remove-from-cart close cls-bt"
                                                                onclick=""
                                                                data-row-id="{{ $item->rowId }}">
                                                            <i class="fas fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="flex justify-content-center">
                                            لا يوجد شيء لعرضه
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                            <div class="row">
                                <div class="basket-bag col-md-8">
                                    <img src="{{ asset('/assets/img/basket-bag.png') }}">
                                </div>
                                @if(\Cart::count())
                                    <div class="col-md-4">
                                        <div class="block">
                                            المجموع: {{\Cart::total()}}
                                        </div>
                                        <div class="text-left ml-5">
                                            <a onclick="requestPay()" class="btn btn-sm btn-primary">
                                                دفع
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script>
        $('.remove-from-cart').on('click', function () {
            $(this).attr('disabled', 'disabled');
            let rawId = $(this).data('row-id');

            $.ajax({
                url: '{{route('remove-from-cart')}}',
                data: {
                    _token: '{{ csrf_token() }}',
                    rawId: rawId,
                },
                type: 'post',
                dataType: "json",
                success: function (response) {
                    location.reload();
                },
                error: function (message) {
                    Swal.fire({
                        icon: 'error',
                        title: 'حدث خطأ',
                        text: message.responseJSON.message,
                        confirmButtonText: 'تم',
                    })
                }
            });
            $(this).removeAttr('disabled');
        });

    </script>
@endsection
