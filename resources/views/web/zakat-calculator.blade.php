@extends('web.layouts.app')
@section('content')

    <script src="https://portal.jod.site/Content/design/js/jquery.js"></script>
    <script src="https://az416426.vo.msecnd.net/scripts/a/ai.0.js"></script>
    <script src="https://portal.jod.site/Scripts/GeneralScript.js"></script>
    <div data-ng-app="myApp" data-ng-controller="myCtrl">
        <section class="header projects-header flex justify-center align-center text-center">
            <div class="container">
                <h5>
                    حاسبة الزكاة
                </h5>

            </div>
        </section>


        <section class="zakatcalculator-sec">
            <div class="container">
                <h3 class="sectionTitle text-center"> حاسبة الزكاة</h3>
                <div class="mb20 text-start">
                    حاسبة الزكاة على موقعنا، تمكنك من حساب قيمة الزكاة الخاصة بك بعد
                    <span class="text-brand">
                        كتابة المال
                    </span>
                    أو المبلغ
                    الذي تملكه بعد تحقق نصاب الزكاة، وكما يمكنك أيضاً من حساب
                    <span class="text-brand">
                        قيمة زكاة
                        الذهب
                    </span>
                    من خلال إدخال
                    مقدار الذهب وبالتالي تتعرف على قيمة الزكاة الواجبة عليها. ويمكنك حساب
                    <span class="text-brand">
                        الزكاة
                        للممتلكات
                        الخاصة
                    </span>
                    بك أو الأسهم أو السندات بكتابة قيمة السهم أو السند، وبعد ذلك يظهر لك قيمة الزكاة
                    الخاصة بها.
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="cal-title-bg text-start">1- زكاة مال</div>
                        <div class="cal-body-bg">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control ZakatAccount" id="Zakat_money"
                                           onkeypress="return isDecimalNumber(event)"
                                           placeholder="ما هي قيمة المال الذي تمتلكه ؟"></div>
                            </div>
                        </div>
                        <div class="cal-title-bg text-start">2- زكاة الأصول والممتلكات</div>

                        <div class="cal-body-bg">
                            <div class="row">
                                <div class="col-md-6"><input type="text" id="Value_shares"
                                                             class="form-control ZakatAccount"
                                                             onkeypress="return isDecimalNumber(event)"
                                                             placeholder="ما هي قيمة الأسهم التي تمتلكها في السوق؟">
                                </div>
                                <div class="col-md-6"><input type="text" id="Zakat_assets_property"
                                                             class="form-control ZakatAccount"
                                                             onkeypress="return isDecimalNumber(event)"
                                                             placeholder="ما هي قيمة الأرباح التي تم الحصول عليها؟">
                                </div>
                            </div>
                        </div>

                        <div class="cal-title-bg text-start">3- زكاة الذهب</div>
                        <div class="cal-body-bg">
                            <div class="row">
                                <div class="col-md-6"><input type="text" id="Zakaah_gold_value_gram"
                                                             class="form-control ZakatAccount"
                                                             onkeypress="return isDecimalNumber(event)"
                                                             placeholder=" أدخل قيمة قيمة جرام الذهب"></div>
                                <div class="col-md-6"><input type="text" id="Zakat_total_gold"
                                                             class="form-control ZakatAccount"
                                                             onkeypress="return isDecimalNumber(event)"
                                                             placeholder=" ما هو وزن الذهب الذي تمتلكه؟"></div>
                            </div>
                        </div>
                        <div class="cal-title-bg text-start">4- زكاة العقارات المملوكة</div>
                        <div class="cal-body-bg">
                            <div class="row">
                                <div class="col-md-6"><input type="text" id="Zakat_real_estate"
                                                             class="form-control ZakatAccount"
                                                             onkeypress="return isDecimalNumber(event)"
                                                             placeholder="ما هو قيمة إيجار العقار السنوي الذي تملكه؟">
                                </div>
                                <div class="col-md-6"><input type="text" id="Market_properties_available_sale"
                                                             class="form-control ZakatAccount"
                                                             onkeypress="return isDecimalNumber(event)"
                                                             placeholder="إجمالي القيمة السوقية للعقارات المتاحة للبيع خلال سنة واحدة">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="mb20 hideMobile"><img src="{{ asset('/assets/img/cal-img-2.jpg') }}"
                                                          class="img-fluid"></div>
                        <div class="mb20 text-bold text-brand">الزكاة الإجمالية المستحقة هي</div>
                        <div>
                            <div class="big-button">
                                <div class="big-button-total" style="padding: 20px;">
                                    <strong>
                                        <span id="total">0.0</span>
                                    </strong>ريال سعودي
                                </div>
                                <!--<div class="big-button-submit"><a href="javascript:void();" onclick="doncatCart()" data-toggle="modal" data-target=".login-popup" >تبرع الآن</a></div>-->
                                <!--<div class="big-button-submit"><a href="javascript:void();" data-toggle="modal" data-target="#modalLoginForm" >تبرع الآن</a></div>-->
                                <div class="big-button-submit">
                                    <a href="javascript:void();" id="modalLoginForm">تبرع الآن</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="openmodalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Donate Fast</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body mx-3">

                    <div class="md-form mb-4">
                        <label data-error="wrong" data-success="right" for="donateAmount"> مبلغ التبرع </label>
                        <input type="text" id="donateAmount" readonly="readonly" class="form-control">
                    </div>

                </div>

                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-primary" onclick="doncatCart()">Login</button>
                </div>
            </div>
        </div>
    </div>
    <script>

        $("#modalLoginForm").click(function () {

            var totalamo = $("#total").text();
            if (totalamo == '0.0') {
                $("#openmodalempty").modal('show');

            } else {
                $("#openmodalLoginForm").modal('show');
                $("#openmodalLoginForm").modal('show');
            }

        });

        function doncatCart() {
            var donateAmount = $("#donateAmount").val();
            var donationType = $("#donationType").val();
            // alert(totalamo); return false;

            if (donateAmount == "" || donateAmount == "0.0") {
                alert("please Enter Price");
                return false;
            } else if (donationType == "" || donationType == "0") {
                alert("please select donation type");
                return false;
            } else {
                // $.noConflict();
                $.ajax({
                    url: '#',
                    data: {_token: '{{ csrf_token() }}', donateAmount: donateAmount, donationType: donationType},
                    type: "POST",
                    success: function (msg) {
                        //   alert(msg); return false;
                        window.location.reload();
                    }
                });
            }

        }

        $(".ZakatAccount").keyup(function (event) {
            var totalmony = 0;
            var Zakaah_gold_value_gram = 0;
            var Zakat_total_gold = 0;
            $('.ZakatAccount').each(function () {
                if (this.id != "Zakaah_gold_value_gram"
                    && this.id != "Zakat_total_gold") {
                    if (this.value != "" && this.value != null)
                        totalmony = parseFloat(totalmony) + parseFloat(this.value);
                } else {
                    if (this.id == "Zakaah_gold_value_gram") {
                        if (this.value != "" && this.value != null) {
                            Zakaah_gold_value_gram = this.value;
                        }
                    } else if (this.id == "Zakat_total_gold")
                        if (this.value != "" && this.value != null) {
                            Zakat_total_gold = this.value;
                        }
                }
            });
            var totalGold = parseFloat(Zakaah_gold_value_gram) * parseFloat(Zakat_total_gold);
            totalmony = parseFloat(totalmony) + parseFloat(totalGold);

            $("#total").text((parseFloat(totalmony) * parseFloat(0.025)).toFixed(1));
            $("#donateAmount").val((parseFloat(totalmony) * parseFloat(0.025)).toFixed(1));
        });


    </script>
@endsection
