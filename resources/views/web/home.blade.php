@extends('web.layouts.app')
@section('content')

    <style>
        .thiredcolor {
            background-color: var(--primary);
            color: #fff;
        }

        .secondarybtn {
            background-color: var(--secondary);

            color: #fff;
            border-radius: 30px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .secondarybtn:hover {
            color: #fff;
            opacity: 0.9;
        }


        .thiredcolor:hover {
            color: #fff;
            opacity: 0.9;
        }

        .card-desgn .title-on-img {
            background-color: var(--primary);
            color: var(--background);
        }

        .projects button.btn.active {
            background-color: var(--primary);
            color: var(--background);
        }

        .msgerrorcon {
            margin-top: 10px;
            display: inline-block;
            width: 100%;
            background-color: #ffe9e9;
            border-right: 3px solid;
            color: #d80f0f;
            padding: 6px 10px;
            line-height: normal;
        }

        .svg-icon-cls svg {
            height: 25px;
            width: auto;
        }

        .svg-icon-cls svg path {
            fill: #fff;
        }

        .addincrem {
            margin: 5px 0 15px;
        }

        .tagboxsele {
            margin-bottom: 0;
        }

        .donate-button {
            position: absolute;
            bottom: 10%;
            right: 5%;
        }
    </style>

    <div class="main-page home-page">
        <section class="home-slide position-relative">
            <div class="container">
                <div class="swiper">
                    <div class="swiper-wrapper">
                        @foreach($sliders as $slider)
                            <div class="swiper-slide">
                                <strong class="title text-uppercase">{{$slider?->title}}</strong>
                                @if($slider->link)
                                    <a href="{{$slider->link}}" target="_blank" class="btn d-bt donate-button">تبرع</a>
                                @endif
                                <img class="img-fluid"
                                     style="background-image: url('{{asset($slider->image_url)}}')"/>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-button-next">
                        <i class="fas fa-angle-right csb"></i>
                    </div>
                    <div class="swiper-button-prev">
                        <i class="fas fa-angle-left csb"></i>
                    </div>
                </div>
            </div>
        </section>
        {{--        @include('web.common.paysidebar')--}}
        @include('web.projects.list')
    </div>
    @include('web.modals.share')
    <script>
        function showShareURL(url) {
            $('#shareModal').modal('show');

            $('#shareModal #ghiras_link_text').val(url);
            $('#shareModal #whatsappShareLink').attr('href', 'https://wa.me/?text=' + url);
            $('#shareModal #twitterShareLink').attr('href', 'https://twitter.com/intent/tweet?url=' + url);
            $('#shareModal #facebookShareLink').attr('href', 'https://www.facebook.com/sharer/sharer.php?u=' + url);
        }

        function copyToClipboard() {
            try {
                var copyText = document.getElementById("ghiras_link_text");
                copyText.select();
                copyText.setSelectionRange(0, 99999); /*For mobile devices*/
                document.execCommand("copy");
            } catch (error) {
            }
        }
    </script>
@endsection
