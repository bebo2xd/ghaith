<div class="sidebar-menu-main">
    <a class="sidebar">
        <div class="side-menu">
            <div class="side-menu-inr" onclick="cartsidebar()" style="cursor: pointer">
                <h2>
                    <a>التبرع السريع</a>
                </h2>
                {{-- <img src="{{asset('/assets/img/side-icon.svg')}}"> --}}
                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="51" viewBox="0 0 56 58">
                <g id="Group_88133" data-name="Group 88133" transform="translate(-4250 -637)">
                    <ellipse id="Ellipse_3" data-name="Ellipse 3" cx="28" cy="29" rx="28" ry="29" transform="translate(4250 637)" fill="{{tenant_theme_color()}}"/>
                    <g id="Icon_Plus" data-name="Icon Plus" transform="translate(4261.708 649.708)">
                    <g id="Outline_2" data-name="Outline 2">
                        <path id="Path_16982" data-name="Path 16982" d="M16.292,0A16.292,16.292,0,1,0,32.584,16.292,16.31,16.31,0,0,0,16.292,0Zm0,30.06A13.768,13.768,0,1,1,30.06,16.292,13.784,13.784,0,0,1,16.292,30.06Z" fill="#fff"/>
                    </g>
                    <g id="Outline_1" data-name="Outline 1" transform="translate(8.72 8.607)">
                        <path id="Path_16983" data-name="Path 16983" d="M150.9,141.548h-5.048V136.5a1.262,1.262,0,1,0-2.524,0v5.048h-5.048a1.262,1.262,0,0,0,0,2.524h5.048v5.048a1.262,1.262,0,1,0,2.524,0v-5.048H150.9a1.262,1.262,0,1,0,0-2.524Z" transform="translate(-137.021 -135.238)" fill="#fff"/>
                    </g>
                    </g>
                </g>
                </svg>
            </div>
        </div>
        <div class="cart-inr-left sidebar-cart">
            <div>
                <div class="cart-info text-center">
                    <h3>المجموع</h3>
                    <div class="cart-info-inr text-center">
                        <p> ريال سعودي </p>

                        <div class="cart-pay-info">
                            <div class="row">
                                <form action="{{route('quick-donate')}}" method="post">
                                    @csrf
                                    <div class="col-md-12">
                                        <div class="form-group gr-form-group gr-form-group-dark gr-form-group-inside con-so-inr">
                                            <input class="form-control" type="text" name="total" placeholder="المبلغ"
                                                   autocomplete="off"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group cart-pay-fo">
                                            <button class="btn" type="submit">أدفع</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>

@section('scripts')
    <script>
        $(function() {
            $('.sidebar-cart').hover(function() {
                $('.side-menu').css('right', '-2px');
            }, function() {
                // on mouseout, reset
                $('.side-menu').css('right', '-150px');
                $('.sidebar-cart').css('display', 'none');
            });

            $('.side-menu').hover(function() {
                $('.side-menu').css('right', '-2px');
            }, function() {
                $('.side-menu').css('right', '-150px');
            });
        });

    </script>
@endsection
