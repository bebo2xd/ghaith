<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="utf-8">

    <!-- Bootstrap CSS -->
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="{!! asset('/styles/fontawesome/css/all.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/boxicons/css/boxicons.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/phone-code/css/intlTelInput.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/styles/theme.css') !!}"/>
{{--    <link rel="stylesheet" href="{!! asset('/styles/responsive.css') !!}"/>--}}
    <link rel="stylesheet" href="{!! asset('/styles/tabs.css') !!}"/>

    <link rel="stylesheet" href="{!! asset('/styles/owl.carousel.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/styles/owl.theme.default.min.css') !!}"/>
    <!--<link rel="stylesheet" href="{!! asset('/styles/pagination.css') !!}" />-->
    {{-- <link rel="stylesheet" href="{!! asset('/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') !!}" /> --}}
    <link rel="stylesheet" href="{!! asset('/styles/style.css') !!}"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    {{-- NEW --}}
    <link rel="stylesheet" href="{!! asset('/assets/css/custom.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/assets/fonts/font.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/assets/css/theme.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/assets/css/responsive.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/assets/css/tabs.css') !!}"/>
    {{-- <link rel="stylesheet" href="{!! asset('/assets/css/style.css') !!}" /> --}}
    <link rel="stylesheet" href="{!! asset('/assets/bootstrap/css/bootstrap-ltr.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/assets/bootstrap/css/bootstrap.rtl.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/assets/plugins/swiper-js/swiper-bundle.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/assets/css/projects.css') !!}"/>

    <link rel="icon" href="{{asset('storage/'.(tenant()?->user?->store?->logo??'logo.svg'))}}">

    <title>{{tenant()?->user?->name??'Ghaith'}}</title>
    <style>
        :root {
            --primary: {{tenant_theme_color()}};
            --primary50: {{tenant_theme_color(50)}};
            --primary25: {{tenant_theme_color(25)}};
            --secondary: #00B140;
            --tertiary: #f6f8fc;
        }

        .navbar-links .nav-link {
            position: relative;
        }

        .countcart {
            position: absolute;
            top: -2px;
            left: 1px;
            background: var(--primary);
            padding: 5px;
            border-radius: 10px;
            font-size: .7rem;
            color: #fff;
        }
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0
        }

        input[type=number] {
            -moz-appearance: none
        }

        .increment-group {
            width: 40%;
        }

        .amount-group {
            width: 40%;
        }

        .amount-group input {
            width: 95%;
        }
        .cursor-pointer{
            cursor: pointer;
        }
        .fixed-top{
            z-index: 10 !important;
        }
        .thiredcolor {
            background-color: var(--primary);
            color: #fff;
        }

        .secondarybtn {
            background-color: var(--secondary) !important;

            color: #fff;
            border-radius: 30px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .secondarybtn:hover {
            color: #fff;
            opacity: 0.9;
        }


        .thiredcolor:hover {
            color: #fff;
            opacity: 0.9;
        }

        .card-desgn .title-on-img {
            background-color: var(--primary);
            color: var(--background);
        }

        .projects button.btn.active {
            background-color: var(--primary);
            color: var(--background);
        }

        .msgerrorcon {
            margin-top: 10px;
            display: inline-block;
            width: 100%;
            background-color: #ffe9e9;
            border-right: 3px solid;
            color: #d80f0f;
            padding: 6px 10px;
            line-height: normal;
        }

        .svg-icon-cls svg {
            height: 25px;
            width: auto;
        }

        .svg-icon-cls svg path {
            fill: #fff;
        }

        .addincrem {
            margin: 5px 0 15px;
        }

        .tagboxsele {
            margin-bottom: 0;
        }

        .donate-button {
            position: absolute;
            bottom: 10%;
            right: 5%;
        }
        .search-box #category{
            font-size: 1rem;
        }
    </style>
</head>
