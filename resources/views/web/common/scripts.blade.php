<script src="{{ asset('/assets/bootstrap/js/jquery.min.js') }}"></script>
<script src="{{ asset('/scripts/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{!! asset('/assets/plugins/swiper-js/swiper-bundle.min.js') !!}"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.3.0.min.js"></script>

<script src="{{ asset('/phone-code/js/intlTelInput.js') }}"></script>

<script src="{{ asset('/scripts/script.js') }}"></script>
<script src="{{ asset('/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
<script>document.getElementsByTagName("html")[0].className += " js";</script>

{{-- NEW JS --}}
<script src="{{ asset('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
{{-- END NEW JS --}}
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    $('#go-up').on('click', function (e) {
        $("html, body").animate({scrollTop: $('body').offset().top, easing: "smooth"}, 300);
        e.preventDefault();
    });

    $(window).on('scroll', (e) => {
        if (window.scrollY > (window.innerHeight / 3)) {
            if (!$('#go-up').hasClass('active')) {
                $('#go-up').addClass('active')
            }
        } else {
            if ($('#go-up').hasClass('active')) {
                $('#go-up').removeClass('active')
            }
        }
    });

    function addToCart(project_id, price, amount, extra = null) {
        $.ajax({
            url: '{{route('add-to-cart')}}',
            data: {
                _token: '{{ csrf_token() }}',
                project_id: project_id,
                price: price,
                amount: amount,
                extra: extra,
            },
            type: 'post',
            dataType: "json",
            success: function (response) {
                $('.countcart').text(response.count);
                Swal.fire({
                    title: 'تمت اضافة المنتج الى السلة',
                    confirmButtonText: 'تم',
                    customClass: {
                        confirmButton: 'bg-theme',
                    },
                })
            },
            error: function (message) {
                Swal.fire({
                    icon: 'error',
                    title: 'حدث خطأ',
                    text: message.responseJSON.message,
                    confirmButtonText: 'تم',
                })
            }
        });
    }

    $('.add-to-cart').on('click', function () {
        $(this).attr('disabled', 'disabled');
        let project_id = $(this).data('project-id');
        let price = $(this).data('price');
        let amount = $(this).data('amount');

        addToCart(project_id, price, amount);
        $(this).removeAttr('disabled');
    });

    $('.add-gift-to-cart').on('click', function () {
        $(this).attr('disabled', 'disabled');
        let modalButton = $('#add-gift-modal-button')

        modalButton.data('project-id', $(this).data('project-id'));
        modalButton.data('price', $(this).data('price'));
        modalButton.data('amount', $(this).data('amount'));

        $('#gift-popup').modal('show');

        $(this).removeAttr('disabled');
    });

    function addGiftToCart(button) {
        let name = $('#personName').val();
        let mobile = $('#personNumber').val();
        if (!name || !mobile) {
            alert('الرجاء تعبئة جميع الحقول')
            return;
        }
        let project_id = $(button).data('project-id');
        let price = $(button).data('price');
        let amount = $(button).data('amount');
        $('#gift-popup').modal('hide');
        let extra = {name: name, mobile: mobile};

        addToCart(project_id, price, amount, extra);
    }

    function requestPay() {
        $.ajax({
            url: '{{route('request-pay')}}',
            data: {
                _token: '{{ csrf_token() }}'
            },
            type: 'post',
            dataType: "json",
            success: function (response) {
                if (!response.status) {
                    Swal.fire({
                        icon: 'error',
                        title: 'حدث خطأ',
                        text: response.message,
                        confirmButtonText: 'تم',
                    });
                    return;
                }
                window.location = response.url;

            },
            error: function (message) {
                Swal.fire({
                    icon: 'error',
                    title: 'حدث خطأ',
                    text: message.responseJSON.message,
                    confirmButtonText: 'تم',
                })
            }
        });
    }
    function cartsidebar() {
        $('.sidebar-cart').toggle();
    }

    function counter(event) {
        if (!event.namespace) {
            return;
        }
        var slides = event.relatedTarget;
        $('.main-banner-inr .owl-dots .owl-dot').attr('data-total-slide', slides.items().length);
    }

    let homeSlideSwiper = new Swiper(".home-slide .swiper", {
        slidesPerView: 1,
        spaceBetween: 0,
        speed: 500,
        loop: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
            pauseOnMouseEnter: true
        },
        navigation: {
            nextEl: ".home-slide .swiper .swiper-button-next",
            prevEl: ".home-slide .swiper .swiper-button-prev"
        },
        mousewheel: false,
        keyboard: false,
    })
</script>
