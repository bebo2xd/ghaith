<footer class="footer">
    <div class="container">
        <div class="f-social-link">
            <ul class="footer-ul">
                <li>
                    <a href="https://www.instagram.com/{{tenant_store('instagram')}}" target="_blank" class="nav-link">
                        <img src="{{ asset('/assets/img/instagram.svg') }}">
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/{{tenant_store('twitter')}}" target="_blank" class="nav-link">
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0,0,256,256">
                            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(5.12,5.12)"><path d="M6.91992,6l14.2168,20.72656l-14.9082,17.27344h3.17773l13.13867,-15.22266l10.44141,15.22266h10.01367l-14.87695,-21.6875l14.08008,-16.3125h-3.17578l-12.31055,14.26172l-9.7832,-14.26172z"></path></g></g>
                        </svg>
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/{{tenant_store('facebook')}}" target="_blank" class="nav-link">
                        <img src="{{ asset('/assets/img/facebook.svg') }}">
                    </a>
                </li>
            </ul>
            <ul class="footer-ul">
                <li>
                    <a href="tel:966501410202" class="nav-link">
                        {{tenant_store('mobile')}}
                        <i class="fas fa-phone"></i>
                    </a>
                </li>
                <li>
                    <a href="mailto:INFO@GHAITH.IO" class="nav-link">
                        {{tenant_store('contact_email')}}
                        <i class="fa fa-envelope"></i>
                    </a>
                </li>
                <li>
                    <a href="#" class="nav-link">
                        RIYADH
                        <i class="fas fa-map-marker-alt"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="f-pay-main">
            <ul class="footer-ul">
                <li><img src="{{ asset('/assets/img/mada.png') }}"></li>
                <li><img src="{{ asset('/assets/img/visa.png') }}"></li>
                <li><img src="{{ asset('/assets/img/a-pay.png') }}"></li>
            </ul>
        </div>
        <div class="f-link">
            <ul class="footer-ul">
                <li>
                    <a href="{{route('home')}}" class="nav-link">الرئيسية</a>
                </li>
                <li>
                    <a href="{{route('projects.index')}}" class="nav-link">المشاريع</a>
                </li>
                <li>
                    <a href="{{route('bank-accounts.index')}}" class="nav-link">الحسابات البنكية</a>
                </li>
                <li>
                    <a href="{{route('zakat-calculator')}}" class="nav-link">حاسبة الزكاة</a>
                </li>
                <li>
                    <a href="{{route('contact-us')}}" class="nav-link">أتصل بنا</a>
                </li>
            </ul>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="footer-copyright text-center flex justify-center align-center">
                        جميع الحقوق محفوظة | {{tenant_store()->user->name}} © {{now()->year}}
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>
