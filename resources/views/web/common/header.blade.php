<nav class="navbar navbar-light navbar-expand-lg fixed-top px-0">
    <div class="container">
        <!-- <button type="button" class="btn menu-toggle d-block d-lg-none"><i class="icon--menu-line"></i></button> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- logo -->
        <a class="navbar-brand header-brand" href="{{ route('home') }}">
            <img
                src="{{ asset(tenant()?->user?->isStore() ? Storage::disk('public')->url(tenant()->user->store->logo) : 'logo.svg') }}">
        </a>
        <!-- navbar menu items -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto align-items-center">
                <!-- menu item : mega menu advanced -->
                <li class="nav-item {{ activeClass('projects.index') }}">
                    <a href="{{ route('projects.index') }}" class="nav-link">
                        مشاريع التبرع
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item {{ activeClass('projects.gift') }}">
                    <a href="{{ route('projects.gift') }}" class="nav-link">الهدايا</a>
                </li>
                <li class="nav-item {{ activeClass('used-furniture-donation') }}">
                    <a href="{{ route('used-furniture-donation') }}" class="nav-link">التبرع بالأثاث</a>
                </li>

                @if (\App\Models\BankAccount::where('store_id', tenant_store_id())->count() > 0)
                <li class="nav-item {{ activeClass('bank-accounts.index') }}">
                    <a href="{{ route('bank-accounts.index') }}" class="nav-link">الحسابات البنكية</a>
                </li>
                @endif
                <li class="nav-item {{ activeClass('zakat-calculator') }}">
                    <a href="{{ route('zakat-calculator') }}" class="nav-link">حاسبة الزكاة</a>
                </li>
                <li class="nav-item {{ activeClass('contact-us') }}">
                    <a href="{{ route('contact-us') }}" class="nav-link">اتصل بنا</a>
                </li>
                @if ($categories->count() > 0)
                <li class="nav-item">
                    <div class="src-box">
                        <div class="form-group src-box-inr">
                            <div class="search-box">
                                <select class="form-control" id="category" name="category_id"
                                    onchange="window.location=this.value">
                                    <option>أختر التصنيف</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ route('project-category.index', $category) }}">
                                            {{ $category->title }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="arrow-down">
                                    <i class="fas fa-arrow-down"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endif
                <li class="nav-item">
                    <div class="src-box">
                        <div class="form-group src-box-inr">
                            <form action="{{ route('projects.index') }}">
                                <div class="search-box">
                                    <input type="search" placeholder="البحث.." class="form-control" name="search"
                                        id="search" placeholder="ابحث هنا…"
                                        onkeypress="if(event.key === 'Enter') this.form.submit">
                                    <i class="fas fa-search search-box-inr"></i>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>
                @if (Auth::guard('web')->check())
                    <li>
                        <a class="nav-link" href="{{ route('profile') }}">
                            <img src="{!! asset('/assets/img/user.svg') !!}">
                        </a>
                    </li>
                    <li class="log-icons-funcs-logout">
                        <a class="nav-link" href="{{ route('logout') }}">
                            <p>خروج</p>
                        </a>
                    </li>
                @else
                    <li>
                        <a class="nav-link cursor-pointer" onclick="Livewire.emit('openModal', 'login-modal')">تسجيل
                            الدخول</a>
                    </li>
                @endif
                <li class="nav-item position-relative">
                    <a class="nav-link" href="{{ route('cart') }}">
                        {{-- <img src="{!! asset('/assets/img/add_cart.svg') !!}"> --}}
                        <span class="countcart">{{ Cart::content()->count() }}</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                            <path
                                d="M24 3l-.743 2h-1.929l-3.474 12h-13.239l-4.615-11h16.812l-.564 2h-13.24l2.937 7h10.428l3.432-12h4.195zm-15.5 15c-.828 0-1.5.672-1.5 1.5 0 .829.672 1.5 1.5 1.5s1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5zm6.9-7-1.9 7c-.828 0-1.5.671-1.5 1.5s.672 1.5 1.5 1.5 1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5z" />
                        </svg>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
