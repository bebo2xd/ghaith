<div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-modal="true"
     aria-labelledby="shareModalLabel" aria-hidden="true">
    <div class="modal-dialog centered" role="document">
        <div class="modal-content border-rounded-15 px-4 py-3" style="border-radius: 25px;">
            <div class="modal-head mb-0">
                <button style="border:none;background:none;" class="close" type="button" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true" style="font-size:40px;">&times;</span>
                </button>
            </div>

            <div class="text-center mt-0 mb-1">
                <img class="rounded mx-auto mb-5 d-block" src="{{asset('image/share-img.svg')}}"
                     alt="twitter-icon">
                <h5 id="shareModal-text" class="text-primary-green "><font style="vertical-align: inherit;"><font
                                style="vertical-align: inherit;">مشاركة عبر وسائل التواصل الاجتماعي</font></font></h5>
            </div>
            <div class="link-label mt-3">
                <i class="fa-2x fa-link fas me-2 text-grey"></i>
                <span class="text font-bold"><font style="vertical-align: inherit;"><font
                                style="vertical-align: inherit;">رابط المشاركة</font></font></span>
            </div>
            <div class="share-link">
                <input style="direction:ltr;margin-left: 5%;" id="ghiras_link_text" type="text" readonly="" value=""
                       aria-label="Share link">
                <button onclick="copyToClipboard()" class="btn btn-primary-green rounded-pill"><font
                            style="vertical-align: inherit;"><font style="vertical-align: inherit;">انسخ
                                                                                                    الرابط</font></font>
                </button>
            </div>
            <div class="social-icons" style="display: flex;position: relative;top: 6px;justify-content: center;">
                <div class="li-whatsapp">
                    <a id="whatsappShareLink" class="watsapp" href="" data-action="" target="_blank" rel="noopener">
                        <i class="fab fa-whatsapp"></i>
                    </a>
                </div>
                <div class="li-twitter">
                    <a id="twitterShareLink" class="twitter" href="" data-action="" target="_blank" rel="noopener">
                        <i class="fab fa-twitter"></i>
                    </a>
                </div>
                <div class="li-facebook">
                    <a id="facebookShareLink" class="facebook" href="" data-action="" target="_blank" rel="noopener">
                        <i class="fab fa-facebook"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function showShareURL(url) {
        $('#shareModal').modal('show');

        $('#shareModal #ghiras_link_text').val(url);
        $('#shareModal #whatsappShareLink').attr('href', 'https://wa.me/?text=' + url);
        $('#shareModal #twitterShareLink').attr('href', 'https://twitter.com/intent/tweet?url=' + url);
        $('#shareModal #facebookShareLink').attr('href', 'https://www.facebook.com/sharer/sharer.php?u=' + url);
    }

    function copyToClipboard() {
        try {
            var copyText = document.getElementById("ghiras_link_text");
            copyText.select();
            copyText.setSelectionRange(0, 99999); /*For mobile devices*/
            document.execCommand("copy");
        } catch (error) {
        }
    }
</script>