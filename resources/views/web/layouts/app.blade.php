<!doctype html>
<html lang="ar" dir="rtl">
<head>
    @include('web.common.meta')
    @livewireStyles
</head>
<body>
    @include('web.common.header')
    @yield('content')
    @include('web.common.footer')
    @include('web.common.scripts')
    <a id="go-up" href="#"><i class="fas fa-angle-up"></i></a>
    @livewireScripts
    @livewire('livewire-ui-modal')
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>

    <div class="modal fade gift-popup pop-design" id="gift-popup"
         tabindex="-1" role="dialog" aria-labelledby="myloginpopup"
         aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="close"
                        data-dismiss="modal">&times;
                </button>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="mail-phone-next mt-4">
                        <div class="form-group text-right mb-4">
                            <lable class="mb-2">الأسم
                                <span
                                        class="red-color">*
                                </span>
                            </lable>
                            <div class="input-group">
                                <input type="text" class="form-control rounded-pill text-start"
                                       name="personName" placeholder=" أدخل الأسم"
                                       id="personName">
                            </div>
                        </div>

                        <div class="form-group text-right mb-4">
                            <lable class="mb-2">رقم الهاتف
                                <span
                                        class="red-color">*
                                </span>
                            </lable>
                            <div class="input-group">
                                <input type="text" class="form-control rounded-pill text-left"
                                       name="personNumber" placeholder="9665xxxxxxxx"
                                       id="personNumber">
                            </div>
                        </div>

                        <div class="form-group m-0">
                            <button
                                    id="add-gift-modal-button"
                                    class="btn bg-theme rounded-pill w-100"
                                    onclick="addGiftToCart(this)">
                                اضافة
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('web.common.paysidebar')

</body>
@yield('scripts')

</html>
