@extends('web.layouts.app')
@section('content')
    <link rel="stylesheet" href="https://cdn.moyasar.com/mpf/1.7.3/moyasar.css">

    <!-- Moyasar Scripts -->
    <script src="https://polyfill.io/v3/polyfill.min.js?features=fetch"></script>
    <script src="https://cdn.moyasar.com/mpf/1.7.3/moyasar.js"></script>



    <!-- cart  -->
    <section class="header ">
        <nav aria-label="breadcrumb" class="breadcrumb-bg">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">الرئيسية</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">السلة</li>
                </ol>
                @if(session('response'))
                    <div class="alert @if(session('response')['status']==true) alert-success @else alert-danger @endif"
                         role="alert">
                        {{session('response')['message']}}
                    </div>
                @endif
            </div>
        </nav>
        <div class="container">

        </div>
    </section>


    <div class="main-page">
        <section class="cart-main">
            <div class="container">
                <div class="mysr-form"></div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script>
        Moyasar.init({
            element: '.mysr-form',
            amount: {{\Cart::total(0,'','') * 100}},
            currency: 'SAR',
            language: 'ar',
            description: 'تبرع للمنصة',
            publishable_api_key: '{{tenant()?->user?->moyasar_publishable_key}}',
            callback_url: '{{route('moyasar.callback')}}',
            methods: [
                'creditcard',
            ],
        });
    </script>
@endsection