<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>طلبات التبرع</title>
    <style>
        body {
            font-family: DejaVu Sans, serif;
            text-align: right;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            direction: rtl;
        }

        th, td {
            border: 1px solid #000;
            padding: 1px;
            text-align: left;
        }
    </style>
</head>
<body>
    {{$donations->count()}}  {{ArPhpLaravel::utf8Glyphs('العدد الكلي')}}
    <br>
    <br>
    <br>
    <div class="table-wrapper">
        <table style="width: 100%">
            <thead>
                <tr>
                    <th>{{ArPhpLaravel::utf8Glyphs('التاريخ')}}</th>
                    <th>{{ArPhpLaravel::utf8Glyphs('الكمية')}}</th>
                    <th>{{ArPhpLaravel::utf8Glyphs('النوع')}}</th>
                    <th>{{ArPhpLaravel::utf8Glyphs('حالة المتبرع به')}}</th>
                    <th>{{ArPhpLaravel::utf8Glyphs('المدينة')}}</th>
                    <th>{{ArPhpLaravel::utf8Glyphs('الحي')}}</th>
                    <th>{{ArPhpLaravel::utf8Glyphs('رقم التواصل')}}</th>
                    <th>{{ArPhpLaravel::utf8Glyphs('اسم المتبرع')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($donations as $donation)
                    <tr>
                        <td>{{ArPhpLaravel::utf8Glyphs($donation->created_at)}}</td>
                        <td>{{ArPhpLaravel::utf8Glyphs($donation->quantity)}}</td>
                        <td>{{ArPhpLaravel::utf8Glyphs($donation->type)}}</td>
                        <td>{{ArPhpLaravel::utf8Glyphs($donation->condition)}}</td>
                        <td>{{ArPhpLaravel::utf8Glyphs($donation->city)}}</td>
                        <td>{{ArPhpLaravel::utf8Glyphs($donation->neighborhood)}}</td>
                        <td>{{ArPhpLaravel::utf8Glyphs($donation->donor_mobile)}}</td>
                        <td>{{ArPhpLaravel::utf8Glyphs($donation->donor_name)}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</body>
</html>
