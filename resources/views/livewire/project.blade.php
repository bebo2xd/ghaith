<div>
    <style>
        @keyframes growProgressBar {
            0%, 33% {
                --pgPercentage: 0;
            }
            100% {
                --pgPercentage: var(--value);
            }
        }

        @property --pgPercentage {
            syntax: '<number>';
            inherits: false;
            initial-value: 0;
        }

        div[role="progressbar"] {
            --size: 5rem;
            --fg: rgb(81, 178, 196);
            --bg: #def;
            --pgPercentage: var(--value);
            animation: growProgressBar 3s 1 forwards;
            width: var(--size);
            height: var(--size);
            border-radius: 50%;
            display: grid;
            place-items: center;
            background: radial-gradient(closest-side, white 80%, transparent 0 99.9%, white 0),
            conic-gradient(var(--fg) calc(var(--pgPercentage) * 1%), var(--bg) 0);
            font-family: Helvetica, Arial, sans-serif;
            font-size: calc(var(--size) / 5);
            color: var(--fg);
        }

        div[role="progressbar"]::before {
            counter-reset: percentage var(--value);
            content: counter(percentage) '%';
        }

        .loader {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            background-color: white;
            width: 100%;
            height: 100%;
            z-index: 100;
            display: flex;
        }

        .lds-ring {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
            margin-right: auto;
            margin-left: auto;
        }

        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 64px;
            height: 64px;
            margin: 8px;
            border: 8px solid black;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: black transparent transparent transparent;
        }

        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }

        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }

        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }

        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        .btn-outline-primary {
            color: #51B2C4;
            border-color: #51B2C4;
            border-radius: 25px;
        }

        .btn-outline-primary:hover, .btn-outline-primary.active {
            background-color: #51B2C4;
            border-color: #51B2C4;
        }
    </style>
    <div class="container">
        <button type="button"
                wire:click="$set('filter_by','all')"
                class="btn btn-outline-primary @if($filter_by=='all') active @endif">
            الكل
        </button>
        @foreach($categories as $category)
            <button
                    wire:click="$set('filter_by',{{$category->id}})"
                    type="button" class="btn btn-outline-primary @if($filter_by==$category->id) active @endif">
                {{$category->title}}
            </button>
        @endforeach
    </div>

    <div class="main-page">
        <section class="project-main">
            <div class="container position-relative">
                <div wire:loading.delay.longest wire:loading.class="loader">
                    <div class="lds-ring">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
                <div class="row" wire:loading.remove>
                    @forelse ($projects as $project)
                        <div class="col-lg-4" style="margin-bottom: 1rem">
                            <x-project :project="$project" wire:key="project-{{ $project->id }}">></x-project>
                        </div>
                    @empty
                        <div class="flex justify-center align-middle align-center" style="min-height: 100px">
                            لا توجد معلومات لعرضها
                        </div>
                    @endforelse
                    {{$projects->links()}}
                </div>
            </div>
        </section>
    </div>

</div>
