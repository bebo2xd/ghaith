<div class="p-lg-4">
    @include('livewire.close-button')
    <div class="pb-6">
        <div class="login-popup-inr p-4">
            <div class="usr-img flex justify-content-center">
                <img src="{{asset('/assets/img/user.png')}}">
            </div>
            <h3>تسجيل الدخول</h3>
            <div class="login-dvdr">
                <p>اختر الطريقة</p>
            </div>
            <div class="mail-phone">
                <a class="w-50" onclick="Livewire.emit('openModal','login-by', {{ json_encode(['loginBy'=>'mobile']) }})">
                    <img src="{{asset('/assets/img/mobile-login.png') }}">
                    <lable>هاتف</lable>
                </a>
                <a class="w-50" onclick="Livewire.emit('openModal','login-by')">
                    <img src="{{asset('/assets/img/mail-login.png') }}">
                    <lable>البريد الإلكتروني</lable>
                </a>
            </div>
        </div>
    </div>
</div>
