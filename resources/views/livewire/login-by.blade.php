<div>
    <div class="p-lg-4">
        @include('livewire.close-button')
        <div class="login-popup-inr">
            <div class="usr-img flex justify-content-center">
                <img src="{{asset('/assets/img/user.png')}}">
            </div>
            <h3>تسجيل الدخول</h3>
            <div class="container my-4">
                <div class="form-group text-right">
                    @if($loginByEmail)
                        <lable class="mb-3 block">
                            البريد الإلكتروني
                            <span class="red-color">*</span>
                        </lable>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <img src="{{asset('/assets/img/mail-login.png')}}">
                                </span>
                            </div>
                            <input type="email" class="form-control text-right sendemail" name="email"
                                   wire:model.lazy="email"
                                   placeholder="your@email.com" id="email" required>
                            @error('email')
                            <span class="invalid-feedback block">{{$message}}</span>
                            @enderror
                        </div>
                    @else
                        <div class="form-group text-right">
                            <lable class="mb-3 block">
                                رقم الهاتف المحمول
                                <span class="red-color">*</span>
                            </lable>
                            <div class="input-group" wire:ignore>
                                <input class="form-control text-left" name="phone"
                                       id="phone-login" type="tel">
                            </div>
                            @error('phoneNumber')
                            <span class="invalid-feedback block">{{$message}}</span>
                            @enderror
                            @if($error_message)
                                <div class="invalid-feedback block">
                                    {{$error_message}}
                                </div>
                            @endif
                        </div>
                    @endif
                </div>
                <div class="form-group my-4">
                    <button type="submit" class="btn w-100 secondarybtn" wire:click="sendActionCode" wire:loading.attr="disabled">
                        <span wire:loading.class="d-none">
                            ارسال رمز التحقق
                        </span>
                        <span wire:loading wire:target="sendActionCode">
                            <div class="spinner-border spinner-border-sm" role="status">
                                <span class="sr-only">جار ارسال الرمز...</span>
                            </div>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script>
        @if(!$loginByEmail)

        let phoneLogin = document.querySelector("#phone-login");
        window.intlTelInput(phoneLogin, {
            initialCountry: '{{$selectedCountry}}',
            utilsScript: "https://cdn.jsdelivr.net/npm/intl-tel-input@17.0.3/build/js/utils.js",
        });

        phoneLogin.addEventListener("countrychange", function () {
            let iti = window.intlTelInputGlobals.getInstance(phoneLogin);
            let selectedCountryData = iti.getSelectedCountryData();
            @this.
            set('dialCode', selectedCountryData.dialCode);
        });

        phoneLogin.addEventListener("change", function () {
            @this.
            set('phoneNumber', this.value);
        });
        @endif
    </script>
</div>
