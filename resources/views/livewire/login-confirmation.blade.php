<div>
    <div class="login-popup-inr p-lg-4">
        <div class="usr-img flex justify-content-center">
            <img src="{!! asset('/assets/img/user.png') !!}">
        </div>
        <h3>تسجيل الدخول</h3>
        @if($error_message)
            <div class="mt-2 alert alert-danger" role="alert">
                {{$error_message}}
            </div>
        @else
            <div class="mt-2 alert alert-success" role="alert">
                تم ارسال رمز التحقق
            </div>
        @endif
        <div class="mail-phone-next mt-1 px-8 py-3">
            @if(!$exists)
                <div class="text-right row">
                    <div class="col-md-12">
                        <lable class="mb-1">
                            البيانات
                            <span class="red-color">*</span>
                        </lable>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group icon-right">
                                <div class="input-group-prepend">
                                    <span class="input-group-text h-full">
                                        <img src="{!! asset('/assets/img/user-tb.png') !!}">
                                    </span>
                                </div>
                                <input type="text" class="form-control"
                                       placeholder="الاسم الاول"
                                       wire:model.lazy="first_name" required
                                       name="first_name" id="first_name">
                            </div>
                            @error('first_name')
                            <span class="red-color mt-2">{{$message}}</span>
                            @else
                                <span class="red-color mt-2">الرجاء كتابة الاسم الحقيقي</span>
                                @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group icon-right">
                                <div class="input-group-prepend">
                                    <span class="input-group-text h-full">
                                        <img src="{!! asset('/assets/img/user-tb.png') !!}">
                                    </span>
                                </div>
                                <input type="text" class="form-control" placeholder="الكنية"
                                       wire:model.lazy="last_name" required
                                       name="last_name" id="last_name">
                            </div>
                            @error('last_name')
                            <span class="red-color mt-2">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            @endif
            <div class="form-group text-right mt-2">
                @if($loginByEmail)
                    <lable class="mb-1">البريد الالكتروني
                        <span class="red-color">*</span>
                    </lable>
                    <input class="form-control text-left"
                           value="{{$user['email']}}"
                           disabled readonly>
                @else
                    <lable class="mb-1">رقم الهاتف
                        <span class="red-color">*</span>
                    </lable>
                    <input id="phone-login-next" class="form-control" name="phone2" type="tel"
                           value="{{$user['phone']}}"
                           placeholder="51 234 5678" disabled>
                    <span class="text-muted mt-2">تأكد من الرقم جيدًا حتى يمكن الاتصال بك لتأكيد الطلب</span>
                @endif
                <div>
                    <lable class="mt-3 mb-1">
                        رمز التحقق
                        <span class="red-color">*</span>
                    </lable>
                    <input class="form-control text-right" name="otp"
                           wire:model.lazy="otp">
                    @error('otp')
                    <span class="red-color mt-2">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group mt-2">
                <button class="site-btn" wire:click="login">دخول</button>
            </div>
        </div>
    </div>
</div>
