<?php

namespace App\Traits;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

trait BelongsToStore
{
    public function store()
    {
        return $this->belongsTo(User::class, 'store_id');
    }

    public static function bootBelongsToStore()
    {
        /** @var User $user */
        $user = tenant()?->user;
        $store = tenant()?->user?->store;

        if ($user?->isStore()) {
            static::addGlobalScope(function (Builder $builder) use ($store) {
                // $builder->where('store_id', $user->id);
                $builder->where('store_id', $store->id);
            });
        }
    }
}
