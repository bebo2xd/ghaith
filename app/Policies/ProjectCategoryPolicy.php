<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ProjectCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectCategoryPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return true;
    }

    public function view(User $user, ProjectCategory $ProjectCategory): bool
    {
        return true;
    }

    public function create(User $user): bool
    {
        return true;
    }

    public function update(User $user, ProjectCategory $ProjectCategory): bool
    {
        return $user->store?->id == $ProjectCategory->store_id;
    }

    public function delete(User $user, ProjectCategory $ProjectCategory): bool
    {
        return $user->store?->id == $ProjectCategory->store_id;
    }

    public function restore(User $user, ProjectCategory $ProjectCategory): bool
    {
        return true;
    }

    public function forceDelete(User $user, ProjectCategory $ProjectCategory): bool
    {
        return true;
    }
}
