<?php

namespace App\Policies;

use App\Models\User;
use App\Models\BankAccount;
use Illuminate\Auth\Access\HandlesAuthorization;

class BankAccountPolicy
{
    use HandlesAuthorization;


    public function update(User $user, BankAccount $BankAccount): bool
    {
        return $user->store?->id == $BankAccount->store_id;
    }

    public function delete(User $user, BankAccount $BankAccount): bool
    {
        return $user->store?->id == $BankAccount->store_id;
    }

}
