<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Slider;
use Illuminate\Auth\Access\HandlesAuthorization;

class SliderPolicy
{
    use HandlesAuthorization;


    public function update(User $user, Slider $Slider): bool
    {
        return $user->store?->id == $Slider->store_id;
    }

    public function delete(User $user, Slider $Slider): bool
    {
        return $user->store?->id == $Slider->store_id;
    }

}
