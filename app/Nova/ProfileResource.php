<?php

namespace App\Nova;

use App\Models\User;
use Laravel\Nova\Http\Requests\NovaRequest;

class ProfileResource extends StoreUser
{
    public static $model = User::class;

    public static $title = 'name';

    public static $search = [
        'id', 'name', 'email', 'role'
    ];

    public static $displayInNavigation = false;
    public static $globallySearchable = false;

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('id', auth()->id());
    }

    public static function label()
    {
        return __('Profile');
    }

    public static function singularLabel()
    {
        return __('Profile');
    }

}
