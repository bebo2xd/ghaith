<?php

namespace App\Nova;

use App\Models\DonateRequest;
use App\Nova\Actions\AcceptDonationRequest;
use App\Nova\Actions\ExportDonationRequest;
use App\Nova\Actions\PublishDonationRequest;
use App\Nova\Actions\RejectDonationRequest;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\ActionRequest;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class DonationRequestResource extends Resource
{
    public static $model = DonateRequest::class;

    public static $title = 'id';
    public static $globallySearchable = false;
    public static $perPageViaRelationship = 10;

    public static $search = [
        'id', 'donor_name', 'donor_mobile', 'city', 'neighborhood', 'condition', 'type',
    ];

    public static function label()
    {
        return __('Donation Requests');
    }

    public static function singularLabel()
    {
        return __('Donation Request');
    }

    public function fields(Request $request): array
    {
        return [
            ID::make()->sortable(),

            Text::make(__('Donor Name'), 'donor_name')
                ->sortable()
                ->rules('required'),

            Text::make(__('Mobile'), 'donor_mobile')
                ->sortable()
                ->rules('required'),

            Text::make(__('City'), 'city')
                ->sortable()
                ->rules('required'),

            Text::make(__('Neighborhood'), 'neighborhood')
                ->sortable()
                ->rules('required'),

            Text::make(__('Condition'), 'condition')
                ->sortable()
                ->rules('required'),

            Text::make(__('Type'), 'type')
                ->sortable()
                ->rules('required'),
//            Select::make(__('Status'), 'status')
//                ->options([
//                    'pending' => 'في الانتظار',
//                    'published' => 'تم النشر',
//                    'rejected' => 'تم الرفض',
//                    'accepted' => 'تمت الموافقة',
//                ])
//                ->displayUsingLabels()
//                ->sortable()
//                ->filterable()
//                ->rules('required'),

            Number::make(__('Quantity'), 'quantity')
                ->sortable()
                ->rules('required', 'integer'),
            Images::make(__('Images'), 'default')
                ->hideFromIndex()
                ->rules('required'),
            Date::make('التاريخ', 'created_at')
                ->filterable(),

        ];
    }

    public function actions(NovaRequest $request)
    {
        return [
            (new ExportDonationRequest()),
            (new AcceptDonationRequest())->showInline()
                ->canSee(function ($request) {
                    if ($request instanceof ActionRequest) {
                        return true;
                    }
                    return $this->resource->status == 'pending';
                }),
            (new PublishDonationRequest($this->model()))->showInline()->canSee(function ($request) {
                if ($request instanceof ActionRequest) {
                    return true;
                }
                return $this->resource->status == 'pending';
            }),
            (new RejectDonationRequest())->showInline()->canSee(function ($request) {
                if ($request instanceof ActionRequest) {
                    return true;
                }
                return $this->resource->status == 'pending';
            }),
            (new DownloadExcel())
                ->withHeadings()
                ->withFilename('طلبات التبرع.xlsx'),
        ];
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public static function authorizeToCreate(Request $request)
    {
        return false;
    }

    public function authorizeToUpdate(Request $request)
    {
        return false;
    }

    public function authorizedToUpdate(Request $request)
    {
        return false;
    }

    public function authorizeToDelete(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public static function authorizedToViewAny(Request $request)
    {
        if (auth()->guest()) {
            return false;
        }

        return auth()->user()->isAdmin() ? true : auth()->user()->store?->used_furniture;
    }

    public function authorizeToView(Request $request)
    {
        return auth()->user()->isAdmin() ? true : auth()->user()->store?->used_furniture;
    }
}