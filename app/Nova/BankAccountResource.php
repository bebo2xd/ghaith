<?php

namespace App\Nova;

use App\Models\BankAccount;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Http\Requests\NovaRequest;

class BankAccountResource extends Resource
{
    public static $model = BankAccount::class;

    public static $title = 'title';

    public static $search = [
        'id', 'title', 'iban', 'logo'
    ];


    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('store_id', auth()->user()->store->id);
    }

    public static function label()
    {
        return __('Bank Accounts');
    }

    public static function singularLabel()
    {
        return __('Bank Account');
    }

    public function fields(Request $request): array
    {
        $field = $this->storeField();
        return [
            ID::make()->sortable(),
            $field,
            Text::make(__('Title'), 'title')
                ->sortable()
                ->rules('required'),

            Text::make(__('Iban'), 'iban')
                ->sortable()
                ->rules('required', 'string', 'max:24')
                ->withMeta(['extraAttributes' => ['maxlength' => 24]]),

            Image::make(__('Logo'), 'logo')
                ->sortable()
                ->creationRules('required', 'image')
                ->updateRules('image'),
        ];
    }
}
