<?php

namespace App\Nova;

use App\Models\ProjectCategory;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Hidden;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class ProjectCategoryResource extends Resource
{
    public static $model = ProjectCategory::class;

    public static $title = 'title';

    public static $search = [
        'id', 'title',
    ];

    public static function label()
    {
        return __('Project Categories');
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('store_id', auth()->user()->store->id);
    }

    public static function singularLabel()
    {
        return __('Project Category');
    }

    public function fields(Request $request): array
    {
        return [
            ID::make()->sortable(),

            Hidden::make('store_id')->default(auth()->user()->store->id),

            Text::make(__('Title'), 'title')
                ->sortable()
                ->rules('required'),
        ];
    }

    public function authorizedToDelete(Request $request)
    {
        /** @var ProjectCategory $resource */
        $resource = $this->resource;
        return $resource?->projects()->doesntExist();
    }
}
