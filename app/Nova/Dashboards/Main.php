<?php

namespace App\Nova\Dashboards;

use App\Nova\Metrics\DonateRequestPerDay;
use App\Nova\Metrics\Stores;
use App\Nova\Metrics\NewUsers;
use App\Nova\Metrics\OrdersPerDay;
use App\Nova\Metrics\ProjectsPerDay;
use App\Nova\Metrics\ProjectPerCategory;
use Laravel\Nova\Dashboards\Main as Dashboard;

class Main extends Dashboard
{
    public function name()
    {
        return __('الرئيسية');
    }

    /**
     * Get the cards for the dashboard.
     *
     * @return array
     */
    public function cards()
    {
        return [
            (new Stores())->canSee(function () {
                return auth()->user()->isAdmin();
            }),
            new NewUsers(),
            new OrdersPerDay(),
            new ProjectsPerDay(),
            new ProjectPerCategory(),
            new DonateRequestPerDay(),
        ];
    }
}
