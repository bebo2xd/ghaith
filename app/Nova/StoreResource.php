<?php

namespace App\Nova;

use App\Models\Store;
use App\Rules\HexRule;
use Laravel\Nova\Fields\ID;
use Outl1ne\NovaSimpleRepeatable\SimpleRepeatable;
use Timothyasp\Color\Color;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\FormData;

class StoreResource extends Resource
{
    public static $model = Store::class;
    public static $globallySearchable = false;
    public static $displayInNavigation = false;

    public static $title = 'id';

    public static $search = [
        'id', 'domain', 'logo', 'license'
    ];


    public static function label()
    {
        return __('Stores');
    }

    public static function singularLabel()
    {
        return __('Store');
    }

    public function fields(Request $request): array
    {
        return [
            ID::make()->hide()->sortable(),

            BelongsTo::make(__('User Id'), 'user', StoreUser::class)
                ->sortable()
                ->rules('required', 'integer'),

            Text::make(__('Mobile'), 'mobile')
                ->sortable()
                ->rules('nullable'),

            Text::make(__('رقم الواتس اب'), 'whatsapp')
                ->sortable()
                ->rules('nullable'),

            Text::make(__('البريد الالكتروني'), 'contact_email')
                ->sortable()
                ->rules('email', 'max:254', 'nullable'),

            Text::make(__('Location'), 'location')
                ->sortable()
                ->rules('nullable'),

            Text::make(__('رابط خرائط Google Maps'), 'google_maps')
                ->sortable()
                ->rules('nullable'),


            Text::make(__('انستقرام'), 'instagram')
                ->sortable()
                ->rules('nullable'),

            Text::make(__('تويتر'), 'twitter')
                ->sortable()
                ->rules('nullable'),

            Text::make(__('فيسبوك'), 'facebook')
                ->sortable()
                ->rules('nullable'),


            Text::make(__('Domain'), 'domain')
                ->sortable()
                ->creationRules('required', 'alpha_dash', 'unique:tenants,id')
                ->canSee(function ($request) {
                    return $request->user()->isAdmin();
                }),
                // ->hideWhenUpdating(),

            Image::make(__('Logo'), 'logo')
                ->deletable(false)
                ->creationRules('required', 'image')
                ->updateRules('image'),

            Color::make(__('Color'), 'color')
                ->slider()
                ->rules('nullable', new HexRule()),

            Select::make(__('Payment Gate'), 'payment_gate')
                ->required()
                ->displayUsingLabels()
                ->options([
                    'myfatoorah' => 'MyFatoorah',
                    'moyasar' => 'Moyasar',
                ])
                ->canSee(function ($request) {
                    return true;
                    return $request->user()->isAdmin();
                }),


            $this->dynamicPaymentField('Api Key', 'myfatoorah_api_key', 'myfatoorah')->canSee(function ($request) {
                return true;
                return $request->user()->isAdmin();
            }),
            $this->dynamicPaymentField('Secret Key', 'moyasar_secret_key', 'moyasar')->canSee(function ($request) {
                return true;
                return $request->user()->isAdmin();
            }),
            $this->dynamicPaymentField('Publishable Key', 'moyasar_publishable_key', 'moyasar')->canSee(function ($request) {
                return true;
                return $request->user()->isAdmin();
            }),

            Date::make(__('Start At'), 'start_at')
                ->sortable()
                ->rules('required', 'date', 'before:end_at')
                ->canSee(function ($request) {
                    return $request->user()->isAdmin();
                }),

            Date::make(__('End At'), 'end_at')
                ->sortable()
                ->rules('required', 'date', 'after:start_at')
                ->canSee(function ($request) {
                    return $request->user()->isAdmin();
                }),

            Text::make(__('License'), 'license')
                ->sortable()
                ->rules('required')
                ->canSee(function ($request) {
                    return $request->user()->isAdmin();
                }),
            Boolean::make(__('Used Furniture'), 'used_furniture')
                ->rules('required'),
            SimpleRepeatable::make(__('أنواع الأثاث'), 'furniture_types', [
                Text::make(__('النوع'), 'type')->rules('required'),
            ])->minRows(1),
        ];
    }


    private function dynamicPaymentField($label, $attribute, $dependOn): Text
    {
        return Text::make(__($dependOn) . ' ' . __($label), $attribute)
            ->hide()
            ->dependsOn(
                ['payment_gate'],
                function (Text $field, NovaRequest $request, FormData $formData) use ($dependOn) {
                    if ($formData->payment_gate === $dependOn) {
                        $field->show()->rules(['required']);
                    }
                }
            );
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public function authorizedToUpdate(Request $request)
    {
        return false;
    }
}
