<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Hidden;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Select;
use Illuminate\Validation\Rules;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\FormData;
use Laravel\Nova\Http\Requests\NovaRequest;

class StoreUser extends Resource
{

    public static $model = \App\Models\User::class;
    public static $title = 'name';
    public static $search = [
        'id', 'name', 'email',
    ];

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('role', 'store');
    }

    public function fields(NovaRequest $request)
    {
        return [
            ID::make()->sortable(),

            Text::make(__('Name'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make(__('Email'), 'email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            Password::make(__('Password'), 'password')
                ->onlyOnForms()
                ->creationRules('required', Rules\Password::defaults())
                ->updateRules('nullable', Rules\Password::defaults()),
            Hidden::make('role')->default('store'),
            Select::make(__('Account Status'), 'is_active')
                ->required()
                ->canSee(function ($request) {
                    return $request->user()->isAdmin();
                })
                ->displayUsingLabels()
                ->options([
                    '0' => 'غير فعال',
                    '1' => 'فعال',
                ]),

            HasOne::make(__('Store information'), 'store', StoreResource::class)
                ->required()
                ->canSee(function ($request) {
                    return $request->resource == 'store-users';
                })
        ];
    }


    public function authorizeToViewAny(Request $request)
    {
        return auth()->user()?->isAdmin();
    }

    public static function authorizedToViewAny(Request $request)
    {
        return auth()->user()?->isAdmin();
    }
    public static function authorizedToCreate(Request $request)
    {
        return auth()->user()?->isAdmin();
    }
}
