<?php

namespace App\Nova\Actions;

use App\Models\DonateRequest;
use App\Models\Project;
use App\Models\ProjectCategory;
use Illuminate\Bus\Queueable;
use Illuminate\Http\UploadedFile;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Http\Requests\NovaRequest;
use Mostafaznv\NovaCkEditor\CkEditor;

class PublishDonationRequest extends Action
{
    use InteractsWithQueue, Queueable;

    public $name = 'نشر';

    public function __construct(public DonateRequest $model)
    {
    }

    /**
     * Perform the action on the given models.
     *
     * @param  ActionFields  $fields
     * @param  Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $models->each(function (DonateRequest $model) use ($fields) {
            /** @var UploadedFile $logo */
            $logo = $fields->get('logo');

            $contents = file_get_contents($logo);
            $name = Str::random().'.'.Str::afterLast($logo,'.');

             Storage::disk('image')->put($name, $contents);

            Project::query()->create([
                'title'               => "تبرع $model->type $model->condition ",
                'is_goalable'         => false,
                'is_giftable'         => false,
                'logo'                => $name,
                'store_id'            => $model->store_id?? auth()->id(),
                'project_category_id' => $fields->get('category'),
                'description'         => $fields->get('description'),
                'donateable'         => $fields->get('donateable'),
                'shares'              => []
            ]);

            $model->update([
                'status' => 'published',
            ]);
        });
    }

    /**
     * Get the fields available on the action.
     *
     * @param  NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        $fields = [
            Select::make('التصنيف', 'category')
                ->options(ProjectCategory::query()->pluck('title', 'id')->toArray())
                ->rules('required'),
            Boolean::make(__('Donateable'), 'donateable')->default(true),
            Select::make(__('Image'), 'logo')
                ->options(function () {
                    $images = [];
                    foreach ($this->model->media as $key => $media) {
                        $i = $key + 1;
                        $images[$media->getFullUrl()] = "الصورة رقم {$i}";
                    }
                    return $images;
                })->withMeta(['placeholder' => 'اختيار صورة, انظر للأسفل لرؤية الصور'])
            ->rules('required'),
            CkEditor::make(__('Image'), 'test')
                ->hideFromIndex()
                ->default(function () {
                    $text = '<p>الرجاء الاختيار من الخيارات التي في الأعلى</p>';
                    foreach ($this->model->media as $key => $media) {
                        $i = $key + 1;
                        $text .= '
                            <p>الصورة رقم '.$i.':</p>
                            <figure class="image ck-widget image-style-align-right ck-widget_selected d-contents">
                                <img class="w-25" src="'.$media->getUrl().'">
                            </figure>
                           <hr>
                            ';
                    }

                    return $text;
                })
                ->toolbar([])
                ->imageBrowser(false)
                ->readonly(true)
                ->rules('required'),

            CkEditor::make(__('Description'), 'description')
                ->hideFromIndex()
                ->rules('required'),
        ];
//        if (auth()->user()->isAdmin()) {
//            $fields[] = Select::make('المتجر', 'store')
//                ->options(User::query()->where('role', 'store')->pluck('name', 'id')->toArray())
//                ->default(function()use ($request){
//                    dd($request,$this);
//                    return 1;
//                })
//                ->rules('required');
//        }
        return $fields;
    }
}
