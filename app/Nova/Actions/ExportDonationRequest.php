<?php

namespace App\Nova\Actions;

use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class ExportDonationRequest extends Action
{
    public $name = 'تحميل pdf';

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $pdf = Pdf::loadView('pdf.donate-report', ['donations' => $models]);
        $pdf->save('donation-file.pdf','public');
        return Action::download(Storage::disk('public')->url('donation-file.pdf'), 'طلبات التبرع.pdf');

    }


}
