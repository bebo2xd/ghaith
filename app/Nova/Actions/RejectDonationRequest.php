<?php

namespace App\Nova\Actions;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Actions\DestructiveAction;

class RejectDonationRequest extends DestructiveAction
{

    public $name = 'رفض';

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $models->each(function ($model) {
            $model->update([
                'status' => 'rejected'
            ]);
            if($model->email){
                Mail::send('emails.donation-rejected', [
                    'store_name' => $model->store->name
                ], function ($message) use ($model) {
                    $message->to($model->email)->subject('رفض طلب التبرع');
                });
            }
        });
    }


}
