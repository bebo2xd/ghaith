<?php

namespace App\Nova;

use App\Models\Project;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\FormData;
use Laravel\Nova\Fields\BelongsTo;
use Mostafaznv\NovaCkEditor\CkEditor;
use Laravel\Nova\Http\Requests\NovaRequest;
use Outl1ne\NovaSimpleRepeatable\SimpleRepeatable;

class ProjectResource extends Resource
{
    public static $model = Project::class;

    public static $title = 'title';

    public static $search = [
        'id', 'logo', 'title', 'description'
    ];

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('store_id', auth()->user()->store->id);
    }

    public static function label()
    {
        return __('Projects');
    }

    public static function singularLabel()
    {
        return __('Project');
    }

    public function fields(Request $request): array
    {
        $field = $this->storeField();
        return [
            ID::make()->sortable(),

            $field,

            BelongsTo::make(__('Project Category'), 'category', ProjectCategoryResource::class)
                ->sortable()
                ->rules('required'),

            Image::make(__('Logo'), 'logo')
                ->sortable()
                ->disk('image')
                ->creationRules('required', 'image')
                ->updateRules('image'),
            Text::make(__('Title'), 'title')
                ->sortable()
                ->rules('required'),
            Boolean::make(__('Giftable'), 'is_giftable'),
            Boolean::make(__('Goalable'), 'is_goalable'),
            Text::make(__('Goal'), 'goal')
                ->hide()
                ->dependsOn(
                    ['is_goalable'],
                    function (Text $field, NovaRequest $request, FormData $formData) {
                        if ($formData->is_goalable) {
                            $field->show()->rules(['required', 'numeric']);
                        }
                    }
                ),
            Number::make(__('Total Donations'),function (Project $project){
                return $project->totalPayment();
            }),
            Boolean::make(__('Donateable'), 'donateable')->hideFromIndex(),
            CkEditor::make(__('Description'), 'description')
                ->hideFromIndex()
                ->rules('required'),
            SimpleRepeatable::make(__('Shares'), 'shares', [
                Text::make(__('Title'), 'title')->rules('required'),
                Text::make(__('Price'), 'price')->rules('required'),
            ]),
        ];
    }
}
