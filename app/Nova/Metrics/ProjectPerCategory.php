<?php

namespace App\Nova\Metrics;

use App\Models\Project;
use App\Models\ProjectCategory;
use Laravel\Nova\Metrics\Partition;
use Laravel\Nova\Http\Requests\NovaRequest;

class ProjectPerCategory extends Partition
{
    public $name = 'عدد المشاريع لكل تصنيف';

    /**
     * Calculate the value of the metric.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return mixed
     */
    public function calculate(NovaRequest $request)
    {
        $query = Project::query()->when($request->user()->isStore(), function ($query) use ($request) {
            $query->where('store_id', $request->user()->id);
        });
        $projectCategories = ProjectCategory::all();

        return $this->count($request, $query, 'project_category_id')
            ->label(function ($value) use ($projectCategories) {
                return $projectCategories->find($value)->title;
            });
    }

    /**
     * Determine the amount of time the results of the metric should be cached.
     *
     * @return \DateTimeInterface|\DateInterval|float|int|null
     */
    public function cacheFor()
    {
//         return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'project-per-category';
    }
}
