<?php

namespace App\Nova;

use Eminiarts\Tabs\Tabs;
use Eminiarts\Tabs\Traits\HasActionsInTabs;
use Eminiarts\Tabs\Traits\HasTabs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Http\Requests\NovaRequest;

class DonateTabs extends Resource
{
    use HasTabs;
    use HasActionsInTabs;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\User::class;
    public static $displayInNavigation = false;
    public static $perPageViaRelationship = 10;
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'email',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            Tabs::make('Some Title', [
                HasMany::make('طلبات جديدة', 'pendingDonationRequests', DonationRequestResource::class),
                HasMany::make('المقبولة', 'acceptedDonationRequests', DonationRequestResource::class),
                HasMany::make('المنشورة', 'publishedDonationRequests', DonationRequestResource::class),
                HasMany::make('المرفوضة', 'rejectedDonationRequests', DonationRequestResource::class),
            ]),
        ];
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public static function authorizeToCreate(Request $request)
    {
        return false;
    }

    public function authorizeToUpdate(Request $request)
    {
        return false;
    }

    public function authorizedToUpdate(Request $request)
    {
        return false;
    }

    public function authorizeToDelete(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public static function authorizedToViewAny(Request $request)
    {
        return false;
    }

    public function authorizeToView(Request $request)
    {
        return auth()->user()->isAdmin() ? true : auth()->user()->store?->used_furniture;
    }
}
