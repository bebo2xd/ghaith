<?php

namespace App\Nova;

use App\Models\Slider;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Http\Requests\NovaRequest;

class SliderResource extends Resource
{
    public static $model = Slider::class;

    public static $title = 'title';


    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('store_id', auth()->user()->store->id);
    }

    public static $search = [
        'id', 'title', 'image'
    ];

    public static function label()
    {
        return __('Sliders');
    }

    public static function singularLabel()
    {
        return __('Slider');
    }

    public function fields(Request $request): array
    {
        $storeField = $this->storeField();
        return [
            ID::make()->sortable(),
            $storeField,

            Text::make(__('Title'), 'title')
                ->sortable()
                ->rules('nullable'),

            Image::make(__('Image'), 'image')
                ->sortable()
                ->disk('image')
                ->creationRules('required', 'image')
                ->updateRules('image'),

            Text::make(__('Link'), 'link')
                ->sortable()
                ->rules('nullable', 'url'),
        ];
    }
}
