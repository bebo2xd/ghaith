<?php

namespace App\Nova;

use App\Models\OrderItem;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\NovaRequest;

class OrderItemResource extends Resource
{
    public static $model = OrderItem::class;
    public static $title = 'id';

    public static $search = [
        'name',
    ];

    public static function label()
    {
        return __('Payments');
    }

    public static function singularLabel()
    {
        return __('Payment');
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->with('project');
    }

    public function fields(Request $request): array
    {
        return [
            ID::make()->sortable(),

            BelongsTo::make(__('Project'), 'project', ProjectResource::class)
                ->sortable()
                ->rules('required', 'integer')
                ->filterable()
                ->viewable(false),

            Number::make(__('Quantity'), 'qty')
                ->sortable()
                ->rules('required', 'numeric'),

            Number::make(__('Price'), 'price')
                ->sortable()
                ->rules('required', 'numeric'),

            Number::make(__('Total'), 'subtotal')
                ->sortable()
                ->rules('required', 'numeric'),
        ];
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public function authorizedToUpdate(Request $request)
    {
        return false;
    }
}
