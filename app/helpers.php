<?php

use App\Models\Store;
use App\Models\User;
use Illuminate\Support\Facades\Http;

if (!function_exists('activeClass')) {
    function activeClass($routeName)
    {
        return Request::url() == route($routeName) ? 'active' : '';
    }
}
if (!function_exists('verificationCode')) {
    function verificationCode()
    {
        $str_result = '0123456789';

        return substr(str_shuffle($str_result), 0, 6);
    }
}

if (!function_exists('sendSms')) {
    function sendSms($mobile, $otp)
    {
        if (!config('app.sms_status')) {
            return true;
        }

        $username = "Ghaithapp";
        $password = "My@Alkaffit@sms999";
        $destinations = $mobile;
        $message = "رمز الدخول للجمعية $otp";
        $sender = "Alkaff-sys";
        $url = "https://www.jawalbsms.ws/api.php/sendsms?user=$username&pass=$password&to=$destinations&message=$message&sender=$sender";

        $response = Http::get($url)
            ->throw()
            ->body();


        $message = explode("|", $response);

        return isset($message[1]);
    }
}

if (!function_exists('tenant_store')) {
    function tenant_store($key = null)
    {
        // $store = Store::where('domain', tenant('id'))->first();
        $store = tenant()->user->store;

        if (tenant('user')->role == 'admin') {
            $store = Store::find(1);
        }

        return $key ? $store[$key] : $store;
        return $store ?: false;
    }
}

if (!function_exists('tenant_theme_color')) {
    function tenant_theme_color($opacity = null)
    {
        $default = '#0993ac';
        $color = tenant_store()->color == '#ffffff' ? $default : tenant_store()->color;

        if ($opacity >= 1 && $opacity <= 99) {
            return $color . $opacity;
        }
        return $color;
    }
}

if (!function_exists('tenant_store_id')) {
    function tenant_store_id()
    {
        return tenant()?->user?->store?->id;
    }
}
