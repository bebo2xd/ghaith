<?php

namespace App\Http\Controllers;

use App\Models\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function index()
    {

    }


    public function store(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $subject = $request->input('subject').' - '.$request->input('message_type');
        $message = $request->input('message');


        ContactUs::query()->create([
            'name'    => $name,
            'email'   => $email,
            'message' => $message,
            'subject' => $subject,
        ]);

        Mail::send('emails.contact-us',
            [
                'name'         => $name,
                'email'        => $email,
                'user_message' => $message
            ], function ($message) use ($email, $subject) {
                $message->from($email);
                $email = tenant()?->user?->email ?? 'harmistest@gmail.com';
                $message->to($email)->subject($subject);
            });


        return back()->with('success', 'تم ارسال رسالتك بنجاح');
    }

}