<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Payments\Moyasar;
use Illuminate\Http\Request;
use App\Payments\MyFatoorah;
use App\Payments\PaymentGateway;

class CartController extends Controller
{
    public function index()
    {
        $cart = \Cart::content()->map(function ($item) {
            $item->project = Project::find($item->id);
            return $item;
        });


        return view('web.cart', [
            'cart' => $cart
        ]);
    }

    public function requestPay()
    {
        $paymentGate = $this->getPaymentGate();

        $response = (new $paymentGate)->prepare();

        return response()->json($response);
    }

    public function store(Request $request)
    {
        $project = Project::findOrFail($request->project_id);

        \Cart::add($project->id, $project->title, $request->amount, $request->price,
            options: $request->extra ?? []);

        return response()->json([
            'count' => \Cart::content()->count(),
        ]);
    }

    public function destroy(Request $request)
    {
        \Cart::remove($request->rawId);

        return true;
    }

    private function getPaymentGate(): PaymentGateway
    {
        return match (tenant()->user?->store?->payment_gate) {
            'myfatoorah' => new MyFatoorah,
            'moyasar' => new Moyasar,
            default => throw new \Exception('Please select a payment gateway'),
        };
    }

    public function quickDonate(Request $request)
    {
        if ($request->missing('total')) {
            return back();
        }
        $cart = \Cart::add(Project::$DONATE_ID, 'التبرع السريع', 1, $request->total);

        $paymentGate = $this->getPaymentGate();

        $response = (new $paymentGate)->prepare();

        if (isset($response['url'])) {
            return redirect($response['url']);
        } else {
            return redirect()->route('cart')->with(['response' => ['status' => false, 'message' => 'يوجد خطأ فني في بوابة الدفع، حاول مرة اخرى لاحقاً']]);
        }


    }
}
