<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Slider;

class HomeController extends Controller
{
    public function index()
    {
        $sliders = Slider::query()->where('store_id', tenant_store_id())->get();

        // $projects = Project::query()->with('category')
        //     ->whereHas('category', function ($query) {
        //         return $query->where('donateable', false);
        //     })
        //     ->take(6)->get();

        // $donationProjects = Project::query()->with('category')
        //     ->whereHas('category', function ($query) {
        //         return $query->where('donateable', true);
        //     })
        //     ->take(6)->get();

        $projects = Project::query()->where('store_id', tenant_store_id())
            ->take(6)->get();

        return view('web.home', [
            'sliders' => $sliders,
            'projects' => $projects,
            'donationProjects' => $projects,
        ]);
    }
}
