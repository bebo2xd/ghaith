<?php

namespace App\Http\Controllers;

use App\Models\Project;

class ProjectController extends Controller
{

    public function index()
    {
        $projects = Project::latest()->paginate();
        return view('web.projects.index', [
            'projects' => $projects
        ]);
    }

    public function show(Project $project)
    {
        return view('web.projects.show', [
            'project' => $project
        ]);
    }

    public function gift()
    {
        $projects = Project::latest()->where('is_giftable', true)->paginate();

        return view('web.projects.index', [
            'projects' => $projects
        ]);
    }
}