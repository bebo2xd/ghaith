<?php

namespace App\Http\Controllers;

use App\Models\ProjectCategory;

class ProjectCategoryController extends Controller
{
    public function index(ProjectCategory $projectCategory)
    {
        $projects = $projectCategory->projects()->where('store_id', tenant_store_id())->latest()->paginate();

        return view('web.projects.index', [
            'projects' => $projects,
            'category_title' => $projectCategory->title
        ]);
    }
}
