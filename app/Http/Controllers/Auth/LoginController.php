<?php

namespace App\Http\Controllers\Auth;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController
{
    public function logout(Request $request)
    {
        auth()->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('home');
    }

    public function login(Request $request)
    {
        $key = $request->get('loginBy', 'email');

        $email = $request->get("verification_$key");
        $verification_code = $request->get('verification_code');

        $user = User::query()
            ->where("$key", $email)
            ->where('otp', $verification_code)
            ->first();

        if (!$user) {
            $response_array['status'] = 'error';
            return json_encode($response_array);
        }

        Auth::login($user, true);

        $response_array['status'] = 'success';

        return json_encode($response_array);

    }
}