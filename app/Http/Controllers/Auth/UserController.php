<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show()
    {
        $orders = Order::query()->where('user_id', auth()->id())->get();

        return view('web.account', [
            'orders' => $orders,
            'user' => auth()->user(),
        ]);
    }

    public function update(Request $request)
    {
        $email_validation = !empty(auth()->user()->email) ? 'required' : 'nullable';
        $phone_validation = !empty(auth()->user()->phone) ? 'required' : 'nullable';

        $request->validate([
            'name' => 'required',
            'email' => $email_validation,
            'phone' => $phone_validation,
        ]);

        auth()->user()->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);
        return redirect(route('profile'))->with('success', 'تم تحديث الحساب بنجاح');
    }
}
