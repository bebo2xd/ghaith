<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DonateRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class DonateRequestsController extends Controller
{

    public function store(Request $request)
    {
        $donor_mobile = $request->donor_mobile;
        $donor_name = $request->donor_name;
        $donationRequest = DonateRequest::query()->create([
            'user_id'      => auth()?->id(),
            'store_id'     => tenant()?->user?->id,
            'quantity'     => $request->quantity,
            'condition'    => $request->condition,
            'donor_name'   => $donor_name,
            'donor_mobile' => $donor_mobile,
            'type'         => $request->type,
            'city'         => $request->city,
            'neighborhood' => $request->neighborhood,
            'furniture_type' => $request->furniture_type,
            'email' => $request->email,
        ]);

        $donationRequest
            ->addMultipleMediaFromRequest(['images'])
            ->each(function ($fileAdder) {
                $fileAdder->toMediaCollection();
            });

        try{
            Mail::send('emails.new-donation-request', [
                'name'         => $donor_name,
                'donor_mobile' => $donor_mobile
            ], function ($message) {
                $to = tenant()->user->email ?? config('mail.from.address');
                $message->to($to)->subject('New donation request');
            });
        }catch (\Exception $exception) {
            Log::error('error for sending email',[
                'exception' => $exception
            ]);
        }

        return redirect()->back()->with('success', 'تم ارسال طلبك بنجاح');
    }

}