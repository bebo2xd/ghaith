<?php

namespace App\Http\Controllers;

use App\Models\BankAccount;

class BankAccountController extends Controller
{
    public function index()
    {
        return view('web.bank-accounts.index', [
            // 'banks' => BankAccount::query()->paginate()
            'banks' => BankAccount::query()->where('store_id', tenant_store_id())->paginate()
        ]);
    }
}
