<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Livewire;
use LivewireUI\Modal\ModalComponent;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Contracts\Tenant;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;

class LoginBy extends ModalComponent
{

    public string $loginBy;
    public string $email = '';
    public bool $loginByEmail;

    public $dialCode;
    public $phoneNumber;
    public $selectedCountry = 'sa';
    public string $error_message = '';


    public function mount($loginBy = 'email')
    {
        $this->loginBy = $loginBy;
        $this->loginByEmail = $this->loginBy == 'email';
    }

    public function sendActionCode()
    {

        $key = $this->loginByEmail ? 'email' : 'phoneNumber';
        $extraRole = $this->loginByEmail ? '|email' : '';

        $this->validate([
            $key => 'required'.$extraRole
        ]);

        try {
            if ($this->loginByEmail) {
                [$exists, $user] = $this->sendEmailVerification();
            } else {
                [$exists, $user] = $this->sendSmsVerification();
            }

            $this->emit('openModal', 'login-confirmation', [
                'loginBy' => $this->loginBy,
                'exists'  => $exists,
                'user'    => $user
            ]);

        } catch (\Exception $exception) {
            $this->error_message = $exception->getMessage();
        }
    }

    public function sendEmailVerification()
    {
        $email = $this->email;

        [$exists, $user, $otp] = $this->setOtpForUser('email', $email);

        $data = [
            'name'         => $user->name,
            'email'        => $user->email,
            'user_message' => $otp
        ];

        Mail::send('emails.email-verification', ["data" => $data], function ($message) use ($email) {
            $message->from('info@cloudsys.sa');
            $message->to($email)->subject('Verification Email');
        });

        return [$exists, $user];
    }

    public function sendSmsVerification()
    {
        $phoneNo = $this->dialCode.$this->phoneNumber;

        [$exists, $user, $otp] = $this->setOtpForUser('phone', $phoneNo);

        $sms_sent = sendSms($phoneNo, $otp);

        if (!$sms_sent) {
            // throw new \Exception("الرجاء إدخال رقم اتصال صحيح");
            $this->error_message = 'الرجاء إدخال رقم اتصال صحيح';
        }

        return [$exists, $user];
    }

    public function render()
    {
        return view('livewire.login-by');
    }

    public static function modalMaxWidth(): string
    {
        return 'md';
    }

    private function setOtpForUser($key, $value): array
    {
        $token = \request('_token');
        $exists = true;

        $user = User::query()
            ->where($key, $value)
            ->firstOr(function () use ($key, $token, &$exists, $value) {
                $exists = false;

                $user = new User();
                $user->name = $value;
                // this will be overridden if the user is login by email
                $user->email = "$value@user.com";
                $user->$key = $value;
                $user->remember_token = $token;
                $user->password = '';
                $user->store_id = tenant()?->user?->id;
                return $user;
            });
        $otp = verificationCode();
        $user->otp = $otp;
        $user->save();

        return [$exists, $user, $otp];
    }
}
