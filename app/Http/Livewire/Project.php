<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\ProjectCategory;

class Project extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $filter_by = 'all';
    public $categories;

    public function mount()
    {
        if (request()->route('projectCategory')) {
            $this->filter_by = request()->route('projectCategory')->id;
        }
        return $this->categories = ProjectCategory::all();
    }

    public function updatingFilterBy()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.project', [
            'projects'   => \App\Models\Project::query()
                ->where('store_id', tenant_store_id())
                ->when($this->filter_by != 'all', function ($query) {
                    $query->where('project_category_id', $this->filter_by);
                })
                ->when(request()->has('search'), function ($query) {
                    $term = request('search');
                    $query->where('title', 'like',"%$term%");
                })
                ->paginate(),
            'categories' => $this->categories
        ]);
    }
}
