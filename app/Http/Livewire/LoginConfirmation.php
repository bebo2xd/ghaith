<?php

namespace App\Http\Livewire;

use App\Models\User;
use LivewireUI\Modal\ModalComponent;
use Illuminate\Support\Facades\Auth;

class LoginConfirmation extends ModalComponent
{
    public string $loginBy;
    public bool $loginByEmail;
    public bool $exists;
    public $user;

    public string $first_name = '';
    public string $last_name = '';
    public string $otp = '';
    public string $error_message = '';

    public function mount($loginBy = 'email', $exists = false, $user = null)
    {
        $this->loginBy = $loginBy;
        $this->loginByEmail = $this->loginBy == 'email';
        $this->exists = $exists;
        $this->user = $user;
    }

    public function login()
    {
        $not_exist_user_validation = [];
        $user = User::find($this->user['id']);

        if (!$this->exists) {
            $not_exist_user_validation = [
                'first_name' => 'required',
                'last_name'  => 'required',
            ];
        }

        $this->validate([
                'otp' => 'required',
            ] + $not_exist_user_validation);

        if ($user->otp != $this->otp) {
            $this->error_message = 'الرجاء التحقق من الرمز المدخل';
        }
        if (!$this->exists) {
            $user->update([
                'name' => $this->first_name.' '.$this->last_name,
            ]);
        }

        if ($user->otp == $this->otp) {
            Auth::login($user);
            return redirect()->route('home');
        }
    }

    public function render()
    {
        return view('livewire.login-confirmation');
    }
}
