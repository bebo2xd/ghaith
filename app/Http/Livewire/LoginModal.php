<?php

namespace App\Http\Livewire;

use LivewireUI\Modal\ModalComponent;

class LoginModal extends ModalComponent
{
    public function render()
    {
        return view('livewire.login-modal');
    }
    public static function modalMaxWidth(): string
    {
        return 'xl';
    }
}