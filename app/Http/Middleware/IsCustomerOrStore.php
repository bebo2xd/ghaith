<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsCustomerOrStore
{

    public function handle(Request $request, Closure $next)
    {
        $logged_user = auth()->user();

        if ($logged_user && $logged_user->role == 'user') {
            return redirect()->route('home');
        }
        return $next($request);
    }
}
