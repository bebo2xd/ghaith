<?php

namespace App\Payments;

use App\Models\Order;
use MyFatoorah\Library\PaymentMyfatoorahApiV2;

class MyFatoorah implements PaymentGateway
{
    public $paymentObject;

    public function __construct()
    {
        $api_key = tenant()?->user?->store?->myfatoorah_api_key ?? config('myfatoorah.api_key');

        if (config('myfatoorah.test_mode')) {
            //test
            $this->paymentObject = new PaymentMyfatoorahApiV2(
                config('myfatoorah.api_key'),
                config('myfatoorah.country_iso'),
                true
            );
        } else {
            //live
            $this->paymentObject = new PaymentMyfatoorahApiV2(
                $api_key,
                config('myfatoorah.country_iso'),
                false
            );
        }
    }

    public function prepare()
    {
        try {
            $paymentMethodId = 0;
            $amount = \Cart::total(0, '', '');

            $curlData = $this->getPayLoadData($amount);
            $data = $this->paymentObject->getInvoiceURL($curlData, $paymentMethodId);

            $this->saveOrder($data['invoiceId'], $amount);

            $response = ['status' => true, 'test_mode' => config('myfatoorah.test_mode'), 'url' => $data['invoiceURL']];
        } catch (\Exception $e) {
            $response = ['status' => false, 'test_mode' => config('myfatoorah.test_mode'), 'message' => $e->getMessage()];
        }
        return $response;
    }

    private function getPayLoadData($amount)
    {
        $callbackURL = route('myfatoorah.callback');

        return [
            'CustomerName'       => auth()?->user()?->name ?? 'Guest User',
            'InvoiceValue'       => $amount,
            'DisplayCurrencyIso' => 'SAR',
            'CallBackUrl'        => $callbackURL,
            'ErrorUrl'           => $callbackURL,
            'SourceInfo'         => 'Laravel ' . app()::VERSION . ' - MyFatoorah Package ' . MYFATOORAH_LARAVEL_PACKAGE_VERSION
        ];
    }

    public function callback()
    {
        try {
            $paymentId = request('paymentId');
            $data = $this->paymentObject->getPaymentStatus($paymentId, 'PaymentId');

            Order::query()
                ->where('transaction_id', $data->InvoiceId)
                ->first()
                ?->update([
                    'response' => $data,
                    'status'   => $data->InvoiceStatus
                ]);
            $status = false;

            if ($data->InvoiceStatus == 'Paid') {
                $message = 'تم الدفع بنجاح, شكرا لك';
                \Cart::destroy();
                $status = true;
            } else {
                if ($data->InvoiceStatus == 'Failed') {
                    $message = 'حدث خطأ في الدفع نتيجة:  ' . $data->InvoiceError;
                } else {
                    if ($data->InvoiceStatus == 'Expired') {
                        $message = 'خطأ في النظام';
                    }
                }
            }

            $response = ['status' => $status, 'message' => $message];
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        return redirect(route('cart'))->with('response', $response);
    }


    private function saveOrder($invoiceId, $amount): void
    {
        Order::query()->create([
            'transaction_id' => $invoiceId,
            'details'        => \Cart::content(),
            'amount'         => $amount,
            'user_id'        => auth()?->id(),
            'payment_gate'   => 'myfatoorah'
        ]);
    }
}
