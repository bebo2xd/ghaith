<?php

namespace App\Payments;

use App\Models\Order;

class Moyasar implements PaymentGateway
{
    public function prepare()
    {
        return [
            'status' => true,
            'url'    => route('moyasar.pay')
        ];
    }


    public function callback()
    {
        $status = request('status');

        Order::query()->create([
            'transaction_id' => request('id'),
            'details'        => \Cart::content(),
            'amount'         => \Cart::total(2, '', ''),
            'response'       => request()->all(),
            'status'         => $status,
            'user_id'        => auth()?->id(),
            'payment_gate'   => 'Moyasar'
        ]);

        if ($status == 'paid') {
            $response = ['status' => true, 'message' => 'تم الدفع بنجاح, شكرا لك'];
            \Cart::destroy();
        } else {
            $response = ['status' => false, 'message' => request('message')];
        }

        return redirect(route('cart'))->with('response', $response);
    }

}
