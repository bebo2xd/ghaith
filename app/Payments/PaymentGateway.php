<?php

namespace App\Payments;

interface PaymentGateway
{
    public function callback();
    public function prepare();
}