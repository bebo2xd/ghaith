<?php

namespace App\Providers;

use App\Models\User;
use App\Nova\BankAccountResource;
use App\Nova\Dashboards\Main;
use App\Nova\OrderItemResource;
use App\Nova\ProjectCategoryResource;
use App\Nova\ProjectResource;
use App\Nova\Resources\Image;
use App\Nova\SliderResource;
use App\Nova\StoreSettings;
use App\Nova\StoreUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Menu\Menu;
use Laravel\Nova\Menu\MenuItem;
use Laravel\Nova\Menu\MenuSection;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        Nova::enableRTL();
        Nova::withoutNotificationCenter();
        Nova::footer(function ($request) {
            return Blade::render('جميع الحقوق محفوظة');
        });
        Nova::serving(function (ServingNova $event) {
            Nova::$jsonVariables['logo'] = auth()->user()?->isStore() ? public_path('/logo.svg') : file_get_contents(realpath(public_path('/logo.svg')));
            Nova::script('custom-script', asset('/custom.js'));
        });

        Nova::userMenu(function (Request $request, Menu $menu) {
            $menu->prepend(
                MenuItem::make(
                    'Profile',
                    "/resources/profiles/{$request->user()->getKey()}/edit"
                )
            );

            return $menu;
        });

        Nova::mainMenu(function (Request $request) {
            return [
                MenuSection::dashboard(Main::class)->icon('chart-bar'),

                MenuSection::make(__('Resources'), [
                    MenuItem::resource(BankAccountResource::class),
                    MenuItem::resource(OrderItemResource::class),
                    MenuItem::resource(SliderResource::class),
                    MenuItem::resource(StoreUser::class),
                    MenuItem::resource(ProjectCategoryResource::class),
                    MenuItem::resource(ProjectResource::class),
                    MenuItem::link('طلبات التبرع', '/resources/donate-tabs/1'),
                    MenuItem::link('اعدادات المتجر', "/resources/store-users/{$request->user()->getKey()}/edit"),
                ])->collapsable(),

                MenuSection::make(__('Media'), [
                    MenuItem::resource(Image::class),
                ])->icon('none')->collapsable(),
            ];
        });

    }

    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    protected function gate()
    {
        Gate::define('viewNova', function (User $user) {
            return $user->isAdmin();
        });
    }

    protected function dashboards()
    {
        return [
            new \App\Nova\Dashboards\Main,
        ];
    }

    public function tools()
    {
        return [];
    }

    public function register()
    {
        //
    }
}
