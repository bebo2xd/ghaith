<?php

namespace App\Providers;

use Livewire\Livewire;
use App\Models\ProjectCategory;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrapFour();

        view()->composer('*', function ($view) {
            // $categories = ProjectCategory::get();
            $categories = ProjectCategory::where('store_id', tenant_store_id())->get();
            $view->with('categories',$categories);
        });

        Livewire::addPersistentMiddleware([
            InitializeTenancyByDomain::class,
        ]);
    }
}
