<?php

namespace App\Models;

use App\Traits\BelongsToStore;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Project extends Model
{
    use BelongsToStore;

    protected $guarded = [];
    protected $casts = [
        'shares' => 'array',
    ];
    public static $DONATE_ID = 99999;

    public static function booted()
    {
        static::creating(function (self $project) {
            if ($project->goal) {
                $project->remaining = $project->goal;
            }
        });
    }

    public function category()
    {
        return $this->belongsTo(ProjectCategory::class, 'project_category_id');
    }

    public function getLogoUrlAttribute()
    {
        return Storage::disk('image')->url($this->logo);
    }

    public function getShortDescriptionAttribute()
    {
        return Str::of($this->description)->stripTags()->limit(50)->value();
    }

    public function isDone()
    {
        return $this->is_done;
    }

    public function getCompletePercentAttribute()
    {
        $completionPercentage = (($this->goal - $this->remaining) / $this->goal) * 100;
        return number_format($completionPercentage);

        // return ($this->this->goal - $this->remaining) / 100;
    }

    public function forDonate()
    {
        return $this->donateable;
    }

    public function totalPayment()
    {
        return OrderItem::query()
            ->where('project_id', $this->id)
            ->sum('subtotal');
    }
}
