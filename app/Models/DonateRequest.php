<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class DonateRequest extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $guarded = [];

    public function store()
    {
        return $this->belongsTo(User::class,'store_id');
    }
}