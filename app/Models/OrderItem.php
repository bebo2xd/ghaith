<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $guarded = [];

    protected static function booted()
    {
        /** @var User $user */
        $user = auth()->user();

        if ($user?->isStore()) {
            static::addGlobalScope(function (Builder $builder) use ($user) {
                $builder->whereHas('order', function (Builder $builder) use ($user) {
                    $builder->where('store_id', $user->id);
                });
            });
        }
    }

    public function project()
    {
        return $this->belongsTo(Project::class)
            ->withDefault([
                'title' => 'تبرع سريع',
            ]);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}