<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    protected $casts = [
        'details' => 'json',
        'response' => 'json',
    ];

    public static function booted()
    {
        static::creating(function (self $order) {
            $order->store_id = tenant()?->user?->id;
        });

        static::saving(function (self $order) {
            if (strtolower($order->status) != 'paid') {
                return;
            }

            foreach ($order->details as $item) {
                $project = Project::find($item['id']);

                if ( !isset($item['id']) || $item['name'] == 'التبرع السريع' || !$project) {
                    continue;
                }


                if ($project->is_goalable) {
                    $project->update([
                        'remaining' => $project->remaining - $item['price'],
                    ]);
                }
            }
        });
        static::saved(function (self $order) {
            if (strtolower($order->status) != 'paid') {
                return;
            }

            foreach ($order->details as $item) {
                $order->saveOrderItem($item);
            }
        });
    }

    public function saveOrderItem($item)
    {
        OrderItem::query()->create([
            'project_id' => $item['id'],
            'order_id' => $this->id,
            'qty' => $item['qty'],
            'price' => $item['price'],
            'subtotal' => $item['subtotal'],
        ]);
    }

    public function orderName()
    {
        return array_values($this->details)[0]['name'] ?? 'تبرع';
    }

    public function orderCount()
    {
        return collect($this->details)->sum('qty');
    }
}
