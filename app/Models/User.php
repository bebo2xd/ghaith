<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;


    protected $guarded = [];
    protected $attributes = [
        'role' => 'user',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static function booted()
    {
        static::creating(function (self $user) {
            if ($user->store) {
                $user->role = 'store';
            }
        });
    }

    public function isAdmin()
    {
        return $this->role == 'admin';
    }

    public function isStore()
    {
        return $this->role == 'store';
    }

    public function store()
    {
        return $this->hasOne(Store::class);
    }

    public function donationRequests()
    {
        return (new DonateRequest())->newQuery();
    }

    public function pendingDonationRequests()
    {
        return $this->donationRequests()->where('status', 'pending');
    }

    public function acceptedDonationRequests()
    {
        return $this->donationRequests()->where('status', 'accepted');
    }

    public function publishedDonationRequests()
    {
        return $this->donationRequests()->where('status', 'published');
    }

    public function rejectedDonationRequests()
    {
        return $this->donationRequests()->where('status', 'rejected');
    }
}
