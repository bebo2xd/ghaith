<?php

namespace App\Models;

use App\Traits\BelongsToStore;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class BankAccount extends Model
{
    use BelongsToStore;

    protected $guarded = [];

    public function getLogoUrlAttribute()
    {
        return Storage::url($this->logo);
    }
}