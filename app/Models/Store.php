<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class Store extends Model
{
    protected $casts = [
        'start_at' => 'date',
        'end_at'   => 'date',
        'furniture_types' => 'json',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected static function booted()
    {
        static::created(function (self $store) {
            $domain = $store->domain;
            $tenant = Tenant::create(['id' => $domain]);
            $store->user()->update(['tenant_id' => $tenant->id]);

            $baseDomain = Str::after(url('/'), '//'); // stores.ghaith.io
            $parts = explode('.', $baseDomain);
            $rootDomain = $parts[count($parts) - 2] . '.' . $parts[count($parts) - 1];

            $tenant->domains()->create(['domain' => "$domain.$rootDomain"]); // {domain}.ghaith.io


            // create parked domain automatically to cpanel with API
            $cpanelHost = 'https://172-104-229-178.ip.linodeusercontent.com:2083';
            $apiToken = '62AASUX7Q6F2W80WJDX0VR2X59OE2R17';
            // Build the API request URL
            $apiUrl = "{$cpanelHost}/json-api/cpanel";
            // Create the Guzzle client
            $client = new Client();
            // Create the parked domain
            $response = $client->get($apiUrl, [
                'query' => [
                    'cpanel_jsonapi_version' => 2,
                    'cpanel_jsonapi_module' => 'AddonDomain',
                    'cpanel_jsonapi_func' => 'addaddondomain',
                    'newdomain' => $domainToPark,
                    'dir' => '/public_html',
                    'subdomain' => $mainDomain,
                ],
                'headers' => [
                    'Authorization' => 'cpanel ' . $apiToken,
                ],
            ]);
        });
    }
}
