<?php

namespace App\Models;

use App\Traits\BelongsToStore;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Slider extends Model
{
    // use BelongsToStore;

    protected $guarded = [];

    public function getImageUrlAttribute()
    {
        return Storage::disk('image')->url($this->image);
    }
}
