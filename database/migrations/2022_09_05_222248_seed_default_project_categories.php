<?php

use Illuminate\Database\Migrations\Migration;

class SeedDefaultProjectCategories extends Migration
{
    public function up()
    {
        \App\Models\ProjectCategory::create([
            'title' => 'كفالة يتيم'
        ]);
        \App\Models\ProjectCategory::create([
            'title' => 'بناء مسجد'
        ]);
    }

}