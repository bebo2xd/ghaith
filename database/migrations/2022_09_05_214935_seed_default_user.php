<?php

use Illuminate\Database\Migrations\Migration;

class SeedDefaultUser extends Migration
{
    public function up()
    {
        \App\Models\User::create([
            'name'      => 'Admin',
            'role'      => 'admin',
            'email'     => 'admin@admin.com',
            'password'  => bcrypt('admin@admin.com'),
            'is_active' => true,
        ]);
    }

}