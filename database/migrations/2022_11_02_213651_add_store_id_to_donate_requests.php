<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    public function up()
    {
        Schema::table('donate_requests', function (Blueprint $table) {
            $table->foreignId('store_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('donate_requests', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
};