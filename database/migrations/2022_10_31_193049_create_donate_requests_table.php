<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    public function up()
    {
        Schema::create('donate_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->string('donor_name');
            $table->string('donor_mobile');
            $table->string('city');
            $table->string('neighborhood');
            $table->json('location')->nullable();
            $table->string('condition');
            $table->string('type');
            $table->integer('quantity');
            $table->foreignId('project_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('donate_requests');
    }
};