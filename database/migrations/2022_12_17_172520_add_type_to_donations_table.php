<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('donate_requests', function (Blueprint $table) {
            $table->string('furniture_type')->nullable();
        });
    }

    public function down()
    {
        Schema::table('donate_requests', function (Blueprint $table) {
            $table->dropColumn('furniture_type');
        });
    }
};