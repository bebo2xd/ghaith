<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->foreignId('store_id');
            $table->foreignId('project_category_id');
            $table->text('logo');
            $table->string('title');
            $table->longText('description');
            $table->boolean('is_giftable')->default(false);
            $table->float('goal')->nullable();
            $table->json('shares');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('projects');
    }
}