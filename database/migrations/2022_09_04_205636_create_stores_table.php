<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('mobile')->nullable();
            $table->text('location')->nullable();
            $table->string('domain');
            $table->string('logo');
            $table->string('color')->nullable();
            $table->string('payment_gate')->nullable();
            $table->text('payment_key')->nullable();
            $table->timestamp('start_at');
            $table->timestamp('end_at');
            $table->text('license');
            $table->boolean('used_furniture');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('stores');
    }
}